﻿using System;

namespace ConsoleApp.MH_DelegatePhotoExample {
    public class PhotoProcessor {
        //    public delegate void PhotoFilterHandler(Photo photo);


        //    public void Process(string path, PhotoFilterHandler photoFilterHandler) {
        public void Process(string path, Action<Photo> photoFilterHandler, Func<Photo, int> testReturnFunc) {
            var photo = Photo.Load(path);
            // var filters = new PhotoFilters();
            // filters.ApplyBrightness(photo);
            // filters.ApplyContrast(photo);
            // filters.Resize(photo);

            photoFilterHandler(photo);
            testReturnFunc(photo);

            photo.Save();
        }
    }
}