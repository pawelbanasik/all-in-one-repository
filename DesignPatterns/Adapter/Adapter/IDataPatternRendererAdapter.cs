﻿using System.Collections.Generic;

namespace Adapter {
    public interface IDataPatternRendererAdapter {
        string ListPatterns(IEnumerable<Pattern> patterns);
    }
}