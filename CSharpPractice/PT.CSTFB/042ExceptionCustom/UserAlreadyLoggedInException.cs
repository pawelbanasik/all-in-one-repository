﻿using System;
using System.Runtime.Serialization;

namespace ConsoleApp._042ExceptionCustom {
    [Serializable]
    public class UserAlreadyLoggedInException : Exception {
        public UserAlreadyLoggedInException() : base() {
        }

        public UserAlreadyLoggedInException(string message) : base(message) {
        }

        public UserAlreadyLoggedInException(string message, Exception innerException) {
        }

        public UserAlreadyLoggedInException(SerializationInfo info, StreamingContext context) : base(info, context) {
        }
    }
}