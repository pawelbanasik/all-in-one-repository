﻿namespace ConsoleApp.WorkflowExercise {
    public class InitilizerWorkflowExercise {
        public void Init() {
            IWorkflow wrs = new WorkflowRegisterService();
            wrs.RegisterActivity(new PrintHelloTask());
            wrs.RegisterActivity(new PrintTestTask());
            WorkflowEngine wfe = new WorkflowEngine();
            wfe.Run(wrs);
        }
    }
}