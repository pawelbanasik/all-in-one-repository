﻿using System;
using System.Collections.Generic;
using System.Linq;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;

namespace CW00.OpenClosePrinciple {
    public interface IBar {
        void TellMeYourNames();
    }

    public class Bar : IBar {
        private readonly IFoo[] foos;

        public Bar(IFoo[] foos) {
            this.foos = foos;
        }

        public void TellMeYourNames() {
            Array.ForEach(foos, foo => foo.SayName());
        }
    }

    public interface IFoo {
        void SayName();
    }

    public abstract class FooBase : IFoo {
        public abstract string Name { get; }

        public void SayName() {
            Console.WriteLine(Name);
        }
    }

    public class Foo1 : FooBase {
        public override string Name {
            get { return "Hardcoded 1"; }
        }
    }

    public class Foo2 : FooBase {
        public override string Name {
            get { return "Hardcoded 2"; }
        }
    }

    public class Installer : IWindsorInstaller {
        public void Install(IWindsorContainer container, IConfigurationStore store) {
            container.Kernel.Resolver.AddSubResolver(new Castle.MicroKernel.Resolvers.SpecializedResolvers.CollectionResolver(container.Kernel));
            container.Register(Components().ToArray());
        }

        private static IEnumerable<IRegistration> Components() {
            yield return Classes
                .FromThisAssembly()
                .IncludeNonPublicTypes()
                .BasedOn<IFoo>()
                .WithService.Base()
                .LifestyleTransient();

            yield return Component
                .For<IBar>()
                .ImplementedBy<Bar>()
                .LifestyleTransient();
        }
    }

    class Program {
        static void Main(string[] args) {

            WindsorContainer container = new WindsorContainer();
            container.Install(new Installer());

            var bar = container.Resolve<IBar>();
            bar.TellMeYourNames();

        }
    }
}
