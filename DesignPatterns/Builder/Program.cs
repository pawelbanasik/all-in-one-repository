﻿using System;

namespace Builder {
    class Program {
        static void Main(string[] args) {
            SandwichMaker sandwichMaker = new SandwichMaker(new MySandwichBuilder());
            sandwichMaker.BuildSandwich();
            Sandwich sandwich1 = sandwichMaker.GetSandwich();
            sandwich1.Display();

            Console.WriteLine();

            SandwichMaker sandwichMaker2 = new SandwichMaker(new ClubSandwichBuilder());
            sandwichMaker2.BuildSandwich();
            Sandwich sandwich2 = sandwichMaker2.GetSandwich();
            sandwich2.Display();

            Console.ReadKey();
        }
    }
}
