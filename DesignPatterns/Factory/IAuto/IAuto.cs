namespace Factory {

    public interface IAuto {
        void TurnOn();
        void TurnOff();
    }
}