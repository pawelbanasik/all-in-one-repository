﻿namespace Prototype {
    class Program {
        static void Main(string[] args) {
            var scraper = new WebPageScraper("http://www.google.com");
            scraper.PrintPageData();
            var scraper2 = scraper.Clone() as WebPageScraper;
            scraper2.PrintPageData();
        }
    }
}
