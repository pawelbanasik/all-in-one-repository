﻿using System.Linq;

namespace Composite {
    class Program {
        static void Main(string[] args) {
            int goldForKill = 1023;
            Person joe = new Person("Joe");
            Person jake = new Person("Jake");
            Person emily = new Person("Emilly");
            Person sophia = new Person("Sophia");
            Person brian = new Person("Brian");
            Person oldBob = new Person("Old Bob");
            Person newBob = new Person("New Bob");
            Group bobs = new Group { Members = { oldBob, newBob } };

            Group developers = new Group {
                Name = "Developers",
                Members = { joe, jake, emily, bobs }
            };

            //List<IParty> parties = new List<IParty> { developers, sophia, brian };

            Group parties = new Group { Members = { developers, sophia, brian } };
            parties.Gold += goldForKill;
            parties.Stats();

            //int amountForEach = goldForKill / parties.Count;
            //int leftOver = goldForKill % parties.Count;

            //foreach (var party in parties) {
            //    party.Gold += amountForEach + leftOver;
            //    leftOver = 0;
            //    party.Stats();
            //}
        }
    }
}
