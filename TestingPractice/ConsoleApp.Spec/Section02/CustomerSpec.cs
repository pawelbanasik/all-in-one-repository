﻿using System;
using ConsoleApp.Section02;
using Machine.Specifications;
using NSubstitute;
using NSubstitute.ExceptionExtensions;

namespace ConsoleApp.Spec.Section02 {
    class CustomerSpec {
    }

    public class CustomerSpecBase {
        protected static Customer customer;
        protected static IDbGateway dbGateway;
        protected static ILogger logger;
        protected static WorkingStatistics workingStatistics;

        Establish context = () => {
            dbGateway = Substitute.For<IDbGateway>();
            logger = Substitute.For<ILogger>();
            workingStatistics = new WorkingStatistics();
        };

    }

    public class When_Calculate_Wage_With_Hourly_Payed : CustomerSpecBase {
        private Because of = () => {
            workingStatistics.PayHourly = true;
            workingStatistics.HourSalary = 10;
            workingStatistics.WorkingHours = 100;

            dbGateway.GetWorkingStatistics(Arg.Any<int>()).ReturnsForAnyArgs(workingStatistics);

            customer = new Customer(dbGateway, logger);
        };

        private It Should_Calculate_Wage_With_The_Proper_Working_Statistics = () => {
            customer.CalculateWage(Arg.Any<int>()).ShouldEqual(1000);
        };

    }

    public class When_Calculate_Wage_Passes_Id : CustomerSpecBase {
        private Because of = () => {
            int id = 13;
            dbGateway.GetWorkingStatistics(id).ReturnsForAnyArgs(workingStatistics);
            customer = new Customer(dbGateway, logger);
            customer.CalculateWage(id);
        };
        private It Should_Pass_Correct_Id = () => { dbGateway.Received().GetWorkingStatistics(13); };
    }


    public class When_Calculate_Wage_Throws_Exception : CustomerSpecBase {
        static Exception exception;
        private Because of = () => {
            exception = Catch.Exception(() =>
                customer.CalculateWage(Arg.Any<int>()).ThrowsForAnyArgs<InvalidOperationException>());
        };

        private It Should_Return_0 = () => {
            customer.CalculateWage(Arg.Any<int>()).ShouldEqual(0);
        };


    };


}

