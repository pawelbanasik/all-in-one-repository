﻿namespace Decorator {
    public class Ham : PizzaDecorator {
        public Ham(Pizza pizza) : base(pizza) {
            Description = "Ham";
        }

        public override double CalculateCost() {
            return _pizza.CalculateCost() + 1.00;
        }

        public override string GetDescription() {
            return $"Description: {_pizza.GetDescription()}, {Description}";
        }
    }
}
