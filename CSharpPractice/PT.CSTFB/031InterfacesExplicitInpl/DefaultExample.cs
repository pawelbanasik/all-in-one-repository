﻿using System;

namespace ConsoleApp._031InterfacesExplicitInpl {
    public class DefaultExample : I1, I2 {
        public void InterfaceMethod() {
            Console.WriteLine("Default method!");
        }

        void I2.InterfaceMethod() {
            Console.WriteLine("2nd one");
        }
    }
}