﻿namespace ConsoleApp.MH_DelegateArrayExample {
    public class Caller {
        public delegate int Transformer(int number);

        public void Convert(int[] array, Transformer transformer) {
            for (int i = 0; i < array.Length; i++) {
                array[i] = transformer(array[i]);
            }


        }
    }
}