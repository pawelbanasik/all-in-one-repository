﻿using System;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Messaging;
using System.Text;

namespace ConsoleApp {
    class OptionalParameters {
        static void Main2(string[] args) {

            // 1st way
            AddNumbersOne(10, 20, 30, 40, 50);
            // AddNumbersOne(10, 20, new object[] {30, 40, 50});

            // 2nd way
            AddNumbersTwo(10, 20, new int[] { 30, 40 });

            // 3rd way
            AddNumbersThree(10, 20);
            AddNumbersThree(10, 20, new int[] { 30, 40, 50 });

            // 4th way
            AddNumbersFourth(10, 20);
            AddNumbersFourth(10, 20, new int[] { 30, 40, 50 });
        }

        // 1st way - params array
        public static void AddNumbersOne(int firstNumber, int secondNumber, params object[] restOfNumbers) {
            int result = firstNumber + secondNumber;

            if (restOfNumbers != null) {
                result += restOfNumbers.Select(i => (int)i).Sum();
            }
            Console.WriteLine($"Sum = {result}");

        }

        // 2nd way - method overloading
        public static void AddNumbersTwo(int firstNumber, int secondNumber) {
            int result = firstNumber + secondNumber;
            Console.WriteLine(result);
        }

        public static void AddNumbersTwo(int firstNumber, int secondNumber, int[] restOfNumbers) {
            int result = firstNumber + secondNumber;

            if (restOfNumbers != null) {
                result += restOfNumbers.Sum();
            }
            Console.WriteLine($"Sum = {result}");
        }

        // 3rd way - optional parameters
        public static void AddNumbersThree(int firstNumber, int secondNumber, int[] restOfNumbers = null) {
            int result = firstNumber + secondNumber;

            if (restOfNumbers != null) {
                result += restOfNumbers.Sum();
            }
            Console.WriteLine($"Sum = {result}");
        }

        // 4th way - optional attribute
        public static void AddNumbersFourth(int firstNumber, int secondNumber, [Optional] int[] restOfNumbers) {
            int result = firstNumber + secondNumber;

            if (restOfNumbers != null) {
                result += restOfNumbers.Sum();
            }
            Console.WriteLine($"Sum = {result}");
        }
    }
}
