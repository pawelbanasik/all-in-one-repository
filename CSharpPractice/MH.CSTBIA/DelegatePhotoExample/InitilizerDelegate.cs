﻿using System;

namespace ConsoleApp.MH_DelegatePhotoExample {
    public class InitilizerDelegate {
        public void Init() {
            var processor = new PhotoProcessor();
            var filters = new PhotoFilters();
            // PhotoProcessor.PhotoFilterHandler filterHandler = filters.ApplyBrightness;
            Action<Photo> filterHandler = filters.ApplyBrightness;
            filterHandler += filters.ApplyContrast;
            filterHandler += RemoveRedEyeFilter;

            Func<Photo, int> testReturnFunc = filters.GiveBackNumber;

            processor.Process("photo.jpg", filterHandler, testReturnFunc);

        }
        public static void RemoveRedEyeFilter(Photo photo) {
            Console.WriteLine("Apply RemoveRedEye.");
        }
    }
}