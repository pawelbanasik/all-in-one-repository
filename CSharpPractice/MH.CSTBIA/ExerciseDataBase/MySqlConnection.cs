﻿using System;

namespace ConsoleApp.MH_DataBaseExercise {
    public class MySqlConnection : DbConnection {
        public DateTime Start { get; private set; }
        public DateTime Stop { get; private set; }
        public MySqlConnection(string connectionString) : base(connectionString) {
        }


        public override void Open() {
            Console.WriteLine("Opening connection to MySQL...");
            Start = DateTime.Now;

        }

        public override void Close() {
            Console.WriteLine("Closing connection to MySQL...");
            Stop = DateTime.Now;
            Timeout = Stop - Start;
            Console.WriteLine("Length of connection: " + Timeout);
        }
    }
}