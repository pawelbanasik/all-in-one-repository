﻿using Machine.Specifications;
using System;

namespace Sticks.Core.Spec {
    public class GameSpecs {
    }

    public class GameSpecsBase {
        protected static Game game1;
        protected static Game game2;
        protected static Game game3;
        protected static Player winner;

        private Establish context = () => {
            game1 = new Game(10, Player.Human);
            game2 = new Game(10, Player.Machine);
            game3 = new Game(10, Player.Human);
        };
    }

    public class When_Creating_Game_With_Less_Than_10_Sticks : GameSpecsBase {
        private static Exception exception;

        private Because of = () => {
            exception = Catch.Exception(() => game1 = new Game(9, Player.Human));
        };

        private It Should_Throw_Exception = () => { exception.ShouldBeOfExactType<ArgumentException>(); };
    }

    public class When_Creating_Game_With_Valid_Parameters : GameSpecsBase {

        private It Should_Create_Game_In_Proper_State = () => {
            game1.NumberOfSticks.ShouldEqual(10);
            game1.Turn.ShouldEqual(Player.Human);
        };
    }

    public class When_Human_Makes_Move_Taking_Invalid_Number_Of_Sticks : GameSpecsBase {
        private static Exception exception;

        private Because of = () => {
            exception = Catch.Exception(() => game1.HumanMakesMove(4));
        };

        private It Should_Throw_Argument_Exception = () => {
            exception.ShouldBeOfExactType<ArgumentException>();
        };
    }

    public class When_There_Is_Machine_Turn_But_Human_Makes_Move : GameSpecsBase {
        private static Exception exception;

        private Because of = () => { exception = Catch.Exception(() => game2.HumanMakesMove(3)); };

        private It Should_Throw_Invalid_Operation_Exception = () => {
            exception.ShouldBeOfExactType<InvalidOperationException>();
        };
    }

    public class When_Human_Makes_Move_In_Correct_Game_State : GameSpecsBase {
        private Because of = () => { game1 = game1.HumanMakesMove(3); };

        private It Should_Subtract_The_Correct_Number_Of_Sticks = () => {
            game1.NumberOfSticks.ShouldEqual(7);
        };

        private It Should_Be_Turn_Of_Machine = () => {
            game1.Turn.ShouldEqual(Player.Machine);
        };
    }

    public class When_Machine_Makes_Move_But_Turn_Is_Human : GameSpecsBase {
        private static Exception exception;

        private Because of = () => { exception = Catch.Exception(() => game1.MachineMakesMove()); };

        private It Should_Throw_Invalid_Operation_Exception = () => {
            exception.ShouldBeOfExactType<InvalidOperationException>();
        };
    }

    public class When_Human_Takes_The_Last_2_Sticks : GameSpecsBase {

    }

    // omitted 6 tests from GameTests
}
