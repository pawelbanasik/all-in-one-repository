﻿using System;
using System.Collections.Generic;

namespace ConsoleApp.ExceptionHandling {
    public class YouTubeApi {
        public List<Video> GetVideos(string user) {
            try {
                // logic
                throw new Exception("Fake exception!");
            } catch (Exception e) {
                throw new YouTubeException("Could not fetch the videos from YouTube!", e);
            }
            return new List<Video>();
        }
    }
}