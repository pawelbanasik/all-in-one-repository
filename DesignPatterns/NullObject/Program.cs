﻿namespace NullObject {
    class Program {
        static void Main(string[] args) {
            AutoRepository autoRepository = new AutoRepository();
            AutomobileBase automobile = autoRepository.Find("mini cooper");
            automobile.Start();
            automobile.Stop();
        }
    }
}
