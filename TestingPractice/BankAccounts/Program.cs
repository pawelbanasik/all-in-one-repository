﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankAccounts {

    public class Account {
        public decimal Balance { get; private set; }
        public string EmailAddress { get; set; }

        public Account(decimal initialBalance) {
            Balance = initialBalance;
        }

        public void Debit(decimal amountToDebit) {
            Balance -= amountToDebit;
        }

        public void Credit(decimal amountToCredit) {
            Balance += amountToCredit;
        }

    }

    public class Transfer {
        private readonly IEmailGateway emailGateway;

        public Transfer(IEmailGateway emailGateway) {
            this.emailGateway = emailGateway;
        }

        public void TransferFunds(Account fromAccount, Account toAccount, decimal amountToTransfer) {
            if (amountToTransfer > fromAccount.Balance) {
                throw new InvalidOperationException(String.Format($"Cannot transfer {amountToTransfer} from this account because the current balance is only {fromAccount.Balance}"));
            }

            fromAccount.Debit(amountToTransfer);
            toAccount.Credit(amountToTransfer);
            emailGateway.SendEmail("Funds transferred", "bdd@bank.com", fromAccount.EmailAddress, String.Format($"Successfully transfered funds in the amoumt of: {amountToTransfer}"));
        }

    }

    public interface IEmailGateway {
        void SendEmail(string subject, string from, string to, string message);
    }

    class Program {
        static void Main(string[] args) {




        }
    }
}
