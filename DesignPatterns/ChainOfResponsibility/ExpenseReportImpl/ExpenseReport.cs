﻿namespace ChainOfResponsibility {
    public class ExpenseReport : IExpenseReport {
        public decimal Total { get; set; }

        public ExpenseReport(decimal total) {
            Total = total;
        }
    }
}
