﻿namespace AbstractFactory {
    public interface IUserFactory {
        IUser CreateUser(string name1, string name2);
        IUserIdentity CreateIdentity();
    }
}
