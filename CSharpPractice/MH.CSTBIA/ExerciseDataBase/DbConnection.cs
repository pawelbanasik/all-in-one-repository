﻿using System;

namespace ConsoleApp.MH_DataBaseExercise {
    public abstract class DbConnection {
        public string ConnectionString { get;  }
        public TimeSpan Timeout { get; set; }
        public bool isConnected { get;  }

        public DbConnection(string connectionString) {
            ConnectionString = connectionString ?? throw new InvalidOperationException("Connection string cannot be null.");
        }

        public abstract void Open();
        public abstract void Close();


    }
}