﻿using System;

namespace ConsoleApp.EventDelegateExample {
    public class VideoEventArgs : EventArgs {
        public Video Video { get; set; }

    }
}