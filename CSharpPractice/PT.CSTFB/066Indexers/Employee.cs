﻿namespace ConsoleApp._066Indexers {
    public class Employee {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Gender { get; set; }

        public Employee(int id, string name, string gender) {
            Id = id;
            Name = name;
            Gender = gender;
        }
    }
}