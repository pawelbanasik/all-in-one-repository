﻿using System;

namespace BuilderWithFluentInterface {
    public class EmailAddress : IContactInfo {
        private string Address { get; }

        public EmailAddress(string address) {
            Address = address ?? throw new ArgumentNullException();
        }

        public override int GetHashCode() {
            return Address.ToLower().GetHashCode();
        }

        public override bool Equals(object obj) {
            EmailAddress email = obj as EmailAddress;
            if (email == null) {
                return false;
            }
            return string.Compare(Address, email.Address, StringComparison.InvariantCultureIgnoreCase) == 0;
        }

        public override string ToString() => $"{Address}";
    }
}
