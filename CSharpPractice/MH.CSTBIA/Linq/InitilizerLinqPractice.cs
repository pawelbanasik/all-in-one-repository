﻿using System;
using System.Linq;

namespace ConsoleApp.MH_LinqPractice {
    public class InitilizerLinqPractice {
        public void Init() {
            var books = new BookRepository().GetBooks().ToList();

            var cheapBooks = books
                .Where(b => b.Price < 10)
                .OrderBy(b => b.Title)
                .Select(b => b.Title).ToList();

            //            cheapBooks.ForEach(Console.WriteLine);

            //            var cheaperBooks =
            //                from b in books
            //                where b.Price < 10
            //                orderby b.Title
            //                select b.Title;

            //            cheaperBooks.ToList().ForEach(Console.WriteLine);


            var book = books.SingleOrDefault(b => b.Title == "ASP .NET MVC++");
            //            Console.WriteLine(book == null);

            var element = books.First(b => b.Title == "C# Advanced Topics");
            var last = books.LastOrDefault(b => b.Title == "C# Advanced Topics");

            //            Console.WriteLine(element.Title + element.Price);
            //            Console.WriteLine(last.Title + last.Price);

            var pagedBooks = books
                .Skip(2)
                .Take(3)
                .ToList();

            pagedBooks.ForEach(b => Console.WriteLine(b.Title + b.Price));

            var priceMax = pagedBooks.Max(b => b.Price);
            var priceMin = pagedBooks.Min(b => b.Price);
            var totalPrice = books.Sum(b => b.Price);

            Console.WriteLine(priceMax);
            Console.WriteLine(priceMin);
            Console.WriteLine(totalPrice);
        }
    }
}