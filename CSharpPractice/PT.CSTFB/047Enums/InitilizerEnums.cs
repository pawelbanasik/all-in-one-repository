﻿using System;

namespace ConsoleApp._047Enums {

    public enum Gender {
        Male,
        Female,
        None
    }

    public class Customer {
        public string Name { get; set; }
        //    public int Gender { get; set; }
        public Gender Gender { get; set; }

        public Customer(string name, Gender gender) {
            Name = name;
            Gender = gender;
        }

        //    public Customer(string name, int gender) {
        //        Name = name;
        //        Gender = gender;
        //}
    }

    public class InitilizerEnums {
        public void Init() {

            Customer customer1 = new Customer("Mark", Gender.Male);
            Customer customer2 = new Customer("Mary", Gender.Female);
            Customer customer3 = new Customer("Sam", Gender.None);

            Customer[] customers = { customer1, customer2, customer3 };

            //            Array.ForEach(customers, customer => Console.WriteLine($"{customer.Name} {GetGender(customer.Gender)}"));
            Array.ForEach(customers, customer => Console.WriteLine($"{customer.Name} {customer.Gender}"));
        }
    }

    /*
        public static string GetGender(int gender) {
            switch (gender) {
                case 0:
                    return "Unknown";
                case 1:
                    return "Male";
                case 2:
                    return "Female";
                default:
                    return "Invalid data";
            }
        }
        */
}
