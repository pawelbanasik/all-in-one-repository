﻿using System.Collections.Generic;

namespace Mediator {
    public abstract class Aircraft {
        public IAirTrafficControl AirTrafficControl { get; }
        public abstract int Ceiling { get; }
        public int CurrentAltitude { get; set; }
        public string CallSign { get; set; }
        public int Altitude {
            get => CurrentAltitude;
            set {
                CurrentAltitude = value;
                AirTrafficControl.ReceiveAircraftLocation(this);
            }
        }

        protected Aircraft(IAirTrafficControl airTrafficControl, string callSign) {
            AirTrafficControl = airTrafficControl;
            CallSign = callSign;
            AirTrafficControl.RegisterAircraftUnderGuidance(this);
        }

        public void Climb(int heightToClimb) {

        }

        public override bool Equals(object obj) {
            var aircraft = obj as Aircraft;
            return aircraft != null &&
                   CallSign == aircraft.CallSign;
        }

        public override int GetHashCode() {
            return 387198136 + EqualityComparer<string>.Default.GetHashCode(CallSign);
        }

        public void WarnOfAirspaceIntrusionBy(Aircraft reportingAircraft) {
            // do something in response
        }
    }
}
