﻿using System;

namespace LazyLoad {
    class Program {
        static void Main(string[] args) {
            Order order = new Order();
            Console.WriteLine(order.PrintLabel());
        }
    }
}