﻿namespace BuilderWithFluentInterface {
    public interface IPrimaryContactHolder {
        ISecondaryContactHolder WithPrimaryContact(IContactInfo contact);
    }
}
