﻿namespace ConsoleApp.WorkflowExercise {
    public interface IActivity {
        void Execute();
    }
}