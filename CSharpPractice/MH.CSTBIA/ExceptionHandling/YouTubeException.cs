﻿using System;

namespace ConsoleApp.ExceptionHandling {
    public class YouTubeException : SystemException {

        public YouTubeException(string message, Exception innerException)
            : base(message, innerException) {

        }
    }
}