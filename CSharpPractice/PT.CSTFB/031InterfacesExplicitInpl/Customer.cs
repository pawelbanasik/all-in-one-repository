﻿using System;

namespace ConsoleApp._031InterfacesExplicitInpl {
    public class Customer : I1, I2 {
        void I1.InterfaceMethod() {
            Console.WriteLine("I2 Interface Method");
        }

        void I2.InterfaceMethod() {
            Console.WriteLine("I1 Interface Method");
        }
    }
}