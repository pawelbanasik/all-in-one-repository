﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqPractice {

    class Program {
        static void Main(string[] args) {

            int[] numbers = { 2, 3, 4, 5 };

            // basic aggregate functions
            int min = numbers
                .Where(n => n % 2 == 0)
                .Min();


            int max = numbers
                .Where(n => n % 2 == 0)
                .Sum();

            int count = numbers
                .Count(n => n % 2 == 0);

            double average = numbers
                .Where(n => n % 2 == 0)
                .Average();

            string[] countries = { "India", "USA", "UK", "Canada", "Australia" };
            int min1 = countries.Min(n => n.Length);

            // the aggregate function
            string all = countries.Aggregate((a, b) => a + ", " + b);

            int factorial = numbers.Aggregate((a, b) => a * b);
            Console.WriteLine(factorial);
            int factorialSeed = numbers.Aggregate(10, ((a, b) => a * b));
            Console.WriteLine(factorialSeed);

        }

    }
}
