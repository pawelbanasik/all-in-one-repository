﻿namespace Mediator {
    public class Embraer190 : Aircraft {
        public Embraer190(IAirTrafficControl airTrafficControl, string callSign) : base(airTrafficControl, callSign) {
        }

        public override int Ceiling => 60000;
    }
}
