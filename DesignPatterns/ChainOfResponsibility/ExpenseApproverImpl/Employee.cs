﻿namespace ChainOfResponsibility {
    public class Employee : IExpenseApprover {
        public string Name { get; set; }
        public decimal ApprovalLimit { get; set; }

        public Employee(string name, decimal approvalLimit) {
            Name = name;
            ApprovalLimit = approvalLimit;
        }

        public ApprovalResponse ApproveExpense(IExpenseReport expenseReport) {
            return expenseReport.Total > ApprovalLimit ? ApprovalResponse.BeyondApprovalLimit : ApprovalResponse.Approved;
        }
    }
}
