﻿using System;

namespace ConsoleApp._066Indexers {
    public class InitilizerIndexers {
        public void Init() {
            Company company = new Company();
            string s = company[3];
            Console.WriteLine(s);

            string i = company["Male"];
            Console.WriteLine(i);

            company["Female"] = "Male";
            company.Employees.ForEach(emp => Console.WriteLine(emp.Gender));
        }

    }
}