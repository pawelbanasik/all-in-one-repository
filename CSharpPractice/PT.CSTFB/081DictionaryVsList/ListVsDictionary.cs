﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp._081DictionaryVsList {
    public class Country {
        public string Code { get; set; }
        public string Name { get; set; }
        public string Capital { get; set; }

        public Country(string code, string name, string capital) {
            Code = code;
            Name = name;
            Capital = capital;
        }

        public override string ToString() {
            StringBuilder sb = new StringBuilder();
            sb.Append($"Code: {Code}, ");
            sb.Append($"Name: {Name}, ");
            sb.Append($"Capital: {Capital}.");
            return sb.ToString();
        }

        public override bool Equals(object obj) {
            var country = obj as Country;
            return country != null &&
                   Code == country.Code &&
                   Name == country.Name &&
                   Capital == country.Capital;
        }

        public override int GetHashCode() {
            var hashCode = 1130292373;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Code);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Name);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Capital);
            return hashCode;
        }
    }

    class ListVsDictionary {
        static void MainListVsDictionary(string[] args) {
            Country country1 = new Country("IND", "INDIA", "New Delhi");
            Country country2 = new Country("USA", "UNITED STATES", "Washington");
            Country country3 = new Country("GBR", "UNITED KINGDOM", "London");
            Dictionary<string, Country> countries = new Dictionary<string, Country>();
            countries.Add(country1.Code, country1);
            countries.Add(country2.Code, country2);
            countries.Add(country3.Code, country3);

            while (true) {
                Console.WriteLine("Please enter country code or exit to leave application:");
                string input = Console.ReadLine().ToUpper();

                if (input == "EXIT") {
                    break;
                }

                if (countries.ContainsKey(input)) {
                    Console.WriteLine(countries[input]);
                } else {
                    Console.WriteLine("No country with this code exists.");
                }


            }

        }
    }
}
