﻿namespace ConsoleApp.WorkflowExercise {
    public class WorkflowEngine {
        public void Run(IWorkflow workflow) {
            workflow.List.ForEach(activity => activity.Execute());
        }

    }
}