﻿namespace Command {
    public interface ICommandFactory {
        string Name { get; }
        string Description { get; }

        ICommand MakeCommand(string[] arguments);
    }
}