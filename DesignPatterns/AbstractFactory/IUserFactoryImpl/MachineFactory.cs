﻿using System;
using System.Collections.Generic;

namespace AbstractFactory {
    public class MachineFactory : IUserFactory {

        private Dictionary<string, Producer> NameToProducer { get; }

        public MachineFactory(Dictionary<string, Producer> nameToProducer) {
            NameToProducer = nameToProducer ?? throw new ArgumentNullException(nameof(nameToProducer));
        }

        private Producer GetProducer(string name) {
            if (!NameToProducer.TryGetValue(name, out Producer producer)) {
                throw new ArgumentException();
            }
            return producer;
        }

        public IUser CreateUser(string name1, string name2) {
            Producer producer = GetProducer(name1);
            return new Machine(producer, name2);
        }

        public IUserIdentity CreateIdentity() {
            return new MacAddress();
        }
    }
}
