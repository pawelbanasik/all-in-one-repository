﻿using System.Collections.Generic;

namespace ConsoleApp.WorkflowExercise {
    public class WorkflowRegisterService : IWorkflow {
        public List<IActivity> List { get; } = new List<IActivity>();

        public void RegisterActivity(IActivity activity) {
            List.Add(activity);
        }
    }
}