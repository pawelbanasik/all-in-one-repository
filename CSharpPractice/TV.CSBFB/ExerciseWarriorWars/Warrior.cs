﻿using System;

namespace ConsoleApp.TV_WarriorWarsExercise {
    public class Warrior {
        public string Name { get; }
        public int Health { get; private set; } = 100;
        public int Shield { get; }
        public int Attack { get; }
        public Weapon Weapon { get; }
        public bool Dead { get; private set; }

        public Warrior(string name, Weapon weapon) {
            Weapon = weapon;
            Name = name;
            Shield = SetRandomShield();
            Attack = (int)Weapon;
            Health += Shield;
            PrintStats();
        }

        private void PrintStats() {
            Console.WriteLine("Warrior " + Name);
            Console.WriteLine("Shield: " + Shield);
            Console.WriteLine("Attack: " + Attack);
            Console.WriteLine($"Total health (health + shield): {100 + Shield}");
        }

        public bool IsDead() {
            if (Health <= 0) {
                Dead = true;
                return true;
            }
            return false;
        }

        public int SetRandomShield() {
            return new Random().Next(10, 30);
        }

        public void AttackOtherWarrior(Warrior warrior) {
            warrior.Health -= Attack;
            Console.WriteLine($"Warrior with name: {warrior.Name} has been attacked by warrior named: {Name}");
            this.PrintStatus();
            warrior.PrintStatus();
        }

        public void PrintStatus() {
            Console.WriteLine($"Warrior {Name} current health is: {Health}");
        }
    }
}