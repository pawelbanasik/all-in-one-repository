﻿using System;

namespace BuilderWithFluentInterface {
    class Program {
        static void Main(string[] args) {
            Person person =
                PersonBuilder
                    .Person()
                    .WithFirstName("Max")
                    .WithLastName("Planck")
                    .WithPrimaryContact(new EmailAddress("max.planck@my-institute.com"))
                    .WithSecondaryContact(new EmailAddress("max@home-of-planck.com"))
                    .AndNoMoreContacts()
                    .Build();
            Console.WriteLine(person);
        }
    }
}
