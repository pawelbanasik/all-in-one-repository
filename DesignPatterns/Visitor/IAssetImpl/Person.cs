﻿using System.Collections.Generic;

namespace Visitor {
    public class Person : IAsset {
        //public List<RealEstate> RealEstates { get; set; } = new List<RealEstate>();
        //public List<BankAccount> BankAccounts { get; set; } = new List<BankAccount>();
        //public List<Loan> Loans { get; set; } = new List<Loan>();
        public List<IAsset> Assets { get; } = new List<IAsset>();

        public void Accept(IVisitor visitor) {
            foreach (var asset in Assets) {
                asset.Accept(visitor);
            }
        }
    }
}