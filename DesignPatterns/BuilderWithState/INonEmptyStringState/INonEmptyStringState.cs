﻿namespace BuilderWithState{
    public interface INonEmptyStringState {
        INonEmptyStringState Set(string value);
        string Get();
    }
}
