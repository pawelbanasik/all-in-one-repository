﻿using System;

namespace NullObject {
    public abstract class AutomobileBase {
        public static NullAutomobile Null { get; } = new NullAutomobile();
        public abstract Guid Id { get; }
        public abstract string Name { get; }

        public abstract void Start();
        public abstract void Stop();

        public class NullAutomobile : AutomobileBase {
            public override Guid Id => Guid.Empty;
            public override string Name => string.Empty;

            public override void Start() {
            }

            public override void Stop() {
            }
        }
    }
}
