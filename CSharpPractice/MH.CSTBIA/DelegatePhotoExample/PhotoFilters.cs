﻿using System;

namespace ConsoleApp.MH_DelegatePhotoExample {
    public class PhotoFilters {
        public void ApplyBrightness(Photo photo) {
            Console.WriteLine("Apply brightness.");
        }

        public void ApplyContrast(Photo photo) {
            Console.WriteLine("Apply contrast.");
        }

        public void Resize(Photo photo) {
            Console.WriteLine("Resize photo.");
        }

        public int GiveBackNumber(Photo photo) {
            Console.WriteLine(5);
            return 5;
        }
    }
}