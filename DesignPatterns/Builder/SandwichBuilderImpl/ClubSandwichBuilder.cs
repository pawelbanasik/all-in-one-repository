﻿using System.Collections.Generic;

namespace Builder {
    public class ClubSandwichBuilder : SandwichBuilder {
        public override void PrepareBread() {
            sandwich.BreadType = BreadType.Dark;
        }

        public override void ApplyMeatAndCheese() {
            sandwich.CheeseType = CheeseType.Gouda;
            sandwich.MeatType = MeatType.Ham;
        }

        public override void AddCondiments() {
            sandwich.HasMayo = true;
            sandwich.IsToasted = false;
            sandwich.HasMustard = false;
        }

        public override void ApplyVegetables() {
            sandwich.Vegetables = new List<string> { "Tomato", "Onion", "Lettuce" };
        }
    }
}
