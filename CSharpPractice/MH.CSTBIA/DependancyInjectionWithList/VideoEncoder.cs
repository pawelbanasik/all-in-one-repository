﻿using System.Collections.Generic;

namespace ConsoleApp.DependancyInjectionWithList {
    public class VideoEncoder {
        private readonly IList<INotificationChannel> notificationChannels = new List<INotificationChannel>();

        public VideoEncoder() {
        }

        public void Encode(Video video) {
            foreach (INotificationChannel channel in notificationChannels) {
                channel.Send(new Message());
            }
        }

        public void RegisterNotificationChannel(INotificationChannel channel) {
            notificationChannels.Add(channel);
        }
    }
}