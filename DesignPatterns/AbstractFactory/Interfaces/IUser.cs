﻿namespace AbstractFactory {
    public interface IUser : ITicketHolder {
        void SetIdentity(IUserIdentity identity);
    }
}
