﻿using System;
using System.Collections.Generic;

namespace BuilderWithFluentInterface {
    public class PersonBuilder : IFirstNameHolder, ILastNameHolder,
        IPrimaryContactHolder, ISecondaryContactHolder, IPersonBuilder {

        private string FirstName { get; set; }
        private string LastName { get; set; }
        private IList<IContactInfo> Contacts { get; set; } = new List<IContactInfo>();
        private IContactInfo PrimaryContact { get; set; }

        public static IFirstNameHolder Person() => new PersonBuilder();

        public ILastNameHolder WithFirstName(string firstName) {
            if (string.IsNullOrEmpty(firstName)) {
                throw new ArgumentException();
            }
            FirstName = firstName;
            return this;
        }

        public IPrimaryContactHolder WithLastName(string lastName) {
            if (string.IsNullOrEmpty(lastName)) {
                throw new ArgumentException();
            }
            LastName = lastName;
            return this;
        }

        public ISecondaryContactHolder WithPrimaryContact(IContactInfo contact) {
            WithSecondaryContact(contact);
            PrimaryContact = contact;
            return this;
        }

        public ISecondaryContactHolder WithSecondaryContact(IContactInfo contact) {
            if (contact == null) {
                throw new ArgumentException();
            }
            if (Contacts.Contains(contact)) {
                throw new ArgumentException();
            }
            Contacts.Add(contact);
            return this;
        }

        public IPersonBuilder AndNoMoreContacts() => this;

        public Person Build() {
            Person person = new Person();
            person.Name = FirstName;
            person.Surname = LastName;

            foreach (IContactInfo contact in Contacts)
                person.ContactsList.Add(contact);

            person.PrimaryContact = PrimaryContact;
            return person;
        }
    }
}
