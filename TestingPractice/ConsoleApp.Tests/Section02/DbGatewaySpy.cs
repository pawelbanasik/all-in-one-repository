﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleApp.Section02;

// spy
namespace ConsoleApp.Tests.Section02 {
    class DbGatewaySpy : IDbGateway {

        private WorkingStatistics ws;
        public int Id { get; private set; }


        public WorkingStatistics GetWorkingStatistics(int id) {
            Id = id;
            return ws;

        }

        public void SetWorkingStatistics(WorkingStatistics ws) {
            this.ws = ws;
        }

        public bool Connected { get; }
    }
}
