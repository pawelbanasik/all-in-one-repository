﻿using ConsoleApp.Section01;
using NUnit.Framework;

namespace ConsoleApp.Tests.Section01 {

    [TestFixture]
    public class CalculatorTests {

        [Test]
        public void AddNumbers_ValidValues_ReturnsCorrectResult() {
            Calculator calculator = new Calculator();

            int actual = calculator.AddNumbers(1, 2);

            int expectedValue = 3;
            Assert.That(actual, Is.EqualTo(expectedValue));


        }

    }
}
