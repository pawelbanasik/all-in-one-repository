﻿using System;

namespace Mediator {
    class Program {
        static void Main(string[] args) {
            var yytCenter = new YytCenter();
            var flight1 = new Airbus321(yytCenter, "AC159");
            var flight2 = new Boeing737200(yytCenter, "WS203");
            var flight3 = new Embraer190(yytCenter, "AC602");

            flight1.Altitude += 1000;

        }
    }
}
