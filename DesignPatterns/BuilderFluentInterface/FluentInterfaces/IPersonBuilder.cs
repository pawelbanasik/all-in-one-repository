﻿namespace BuilderWithFluentInterface {
    public interface IPersonBuilder {
        Person Build();
    }
}
