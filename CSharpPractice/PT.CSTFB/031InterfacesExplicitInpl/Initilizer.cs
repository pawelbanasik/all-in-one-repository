﻿namespace ConsoleApp._031InterfacesExplicitInpl {
    public class Initilizer {
        public void Init() {
            //            ((I1)program).InterfaceMethod();
            //            ((I2)program).InterfaceMethod();
            I1 i1 = new Customer();
            I2 i2 = new Customer();
            i1.InterfaceMethod();
            i2.InterfaceMethod();

            // default
            DefaultExample de = new DefaultExample();
            de.InterfaceMethod();
        }
    }
}