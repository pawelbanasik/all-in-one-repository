﻿namespace BuilderWithFluentInterface {
    public interface ILastNameHolder {
        IPrimaryContactHolder WithLastName(string surname);
    }
}
