﻿using System;

namespace ConsoleApp.MH_DelegateArrayExample {
    public class InitForDelegateArrayExample {
        public void Init() {
            Caller caller = new Caller();
            int[] array = { 1, 2, 3, 4, 5 };
            caller.Convert(array, new NumberConverter().Multiply);
            Array.ForEach(array, Console.WriteLine);
        
        }
    }
}