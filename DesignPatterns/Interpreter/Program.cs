﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Interpreter {
    // BNF:
    // <sandwhich> ::= <bread> <condimentList> <ingredientList> <condimentList> <bread>
    // <condimentList> ::= { <condiment> }
    // <ingredientList> ::= { <ingredient> }
    // <bread> ::= <whiteBread> | <wheatBread>
    // <condiment> ::= <mayoCondiment> | <mustardCondiment> | <ketchupCondiment>
    // <ingredient> ::= <lettuceIngredient> | <tomatoIngredient> | <chickenIngredient>
    class Program {
        static void Main(string[] args) {

            var sandwhich = new Sandwhich(
                new WheatBread(),
                new CondimentList(
                    new List<ICondiment> { new MayoCondiment(), new MustardCondiment() }),
                new IngredientList(
                    new List<IIngredient> { new LettuceIngredient(), new ChickenIngredient() }),
                new CondimentList(new List<ICondiment> { new KetchupCondiment() }),
                new WheatBread());

            sandwhich.Interpret(new Context());
        }
    }
}
