using System.Collections.Generic;
using System.Linq;

namespace Adapter {
    public class PatternRenderer {
        private readonly IDataPatternRendererAdapter _dataPatternRendererAdapter;

        public PatternRenderer(IDataPatternRendererAdapter dataPatternRendererAdapter) {
            _dataPatternRendererAdapter = dataPatternRendererAdapter;
        }

        public string ListPatterns(IEnumerable<Pattern> patterns) {
            return _dataPatternRendererAdapter.ListPatterns(patterns);
        }

        // hack for test
        public PatternRenderer() : this(new DataPatternRendererAdapter()) {
        }
    }
}