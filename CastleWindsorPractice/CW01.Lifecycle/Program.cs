﻿using Castle.Components.DictionaryAdapter.Xml;
using Castle.Core;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Castle.Windsor.Installer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using Component = Castle.MicroKernel.Registration.Component;

namespace CW01.Lifecycle {

    public interface IOutputService : IDisposable {
        void PrintAllElements<T>(List<T> listOfElements);
    }

    public class OutputServiceImpl : IOutputService, IInitializable, ISupportInitialize {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public void PrintAllElements<T>(List<T> listOfElements) {
            listOfElements.ForEach(e => log.Info(e));
        }

        public void Dispose() {
            log.Warn($"Disposed of {GetType()}, Date: {DateTime.Now}");
        }

        public void Initialize() {
            log.Fatal($"Initialize invoked: {DateTime.Now}");
        }

        public void BeginInit() {
            log.Error($"BeginInit invoked {DateTime.Now}");
        }

        public void EndInit() {
            log.Error($"EndInit invoked {DateTime.Now}");
        }
    }

    public class Student {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public Student(string firstName, string lastName) {
            FirstName = firstName;
            LastName = lastName;
        }

        public override bool Equals(object obj) {
            var student = obj as Student;
            return student != null &&
                   FirstName == student.FirstName &&
                   LastName == student.LastName;
        }

        public override int GetHashCode() {
            var hashCode = 1938039292;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(FirstName);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(LastName);
            return hashCode;
        }

        public override string ToString() {
            StringBuilder sb = new StringBuilder();
            sb.Append($"FirstName: {FirstName}, ");
            sb.Append($"LastName: {LastName}.");
            return sb.ToString();
        }
    }

    //        [Transient]
    [Singleton]
    public class Controller {
        private readonly IOutputService outputService;
        public DateTime CreatedTimeStamp { get; }

        public Controller(IOutputService outputService, DateTime createdTimeStamp) {
            this.outputService = outputService;
            CreatedTimeStamp = createdTimeStamp;
        }

        public List<Student> Students { get; } = new List<Student>() {
            new Student("Pawel", "Banasik"),
            new Student("Grzegorz", "Banasik"),
            new Student("Michal", "Banasik")
        };

        public void PrintAllStudents() {
            outputService.PrintAllElements(Students);
        }
    }

    public class MyInstaller : IWindsorInstaller {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        public void Install(IWindsorContainer container, IConfigurationStore store) {
            container.Register(
                Component.For<IOutputService>()
                    .ImplementedBy<OutputServiceImpl>()
                    .OnCreate(outputService => log.Debug($"Created: {outputService.GetComponentType()}, Date: {DateTime.Now}"))
                    .OnDestroy(outputService => log.Debug($"{outputService.GetComponentType()} is destroyed, Date: {DateTime.Now}")
                    ),
                Component.For<Controller>()
                    .DynamicParameters((kernel, parameters) => parameters["createdTimestamp"] = DateTime.Now)
            );
        }
    }

    class Program {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        static void Main(string[] args) {

            

            WindsorContainer container = new WindsorContainer();
            container.Install(FromAssembly.This());
            Controller controller = container.Resolve<Controller>();
            controller.PrintAllStudents();

            // checking lifestyles by printing hashcode of two Controller objects
            log.Info($"Singleton or Transient?: {controller.GetHashCode()}");

            Controller controller2 = container.Resolve<Controller>();
            log.Info($"Singleton or Transient?: {controller2.GetHashCode()}");

            // Dynamic parameters or config file or in code or arguments or typed factories (for user inuput)
            DateTime controllerCreatedTimeStamp = controller.CreatedTimeStamp;
            log.Warn($"Controller dynamic parameters: {controllerCreatedTimeStamp.GetType()}: {controllerCreatedTimeStamp}");
            container.Dispose();

        }
    }
}
