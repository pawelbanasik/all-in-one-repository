﻿using System;

namespace LazyLoad {
    public class Order {
        public Lazy<Customer> CustomerInitilizer { get; } = new Lazy<Customer>(() => new Customer());
        public Customer Customer => CustomerInitilizer.Value;

        public string PrintLabel() {
            return Customer.CompanyName + "\n" + Customer.Address;
        }
    }
}