﻿namespace ConsoleApp.MH_DelegateArrayExample {
    public class NumberConverter {
        public int Multiply(int number) {
            return number * number;
        }

        public int Add(int number) {
            return number + number;
        }
    }
}