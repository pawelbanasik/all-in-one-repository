﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NSubstitute;
using NSubstitute.ExceptionExtensions;
using NUnit.Framework;
using NUnit.Framework.Internal;

namespace BankAccounts.Tests {
    [TestFixture]
    public class TransferTests {

        private IEmailGateway emailGateway;
        private Transfer transfer;
        private Account fromAccount;
        private Account toAccount;

        [SetUp]
        public void Setup() {
            emailGateway = Substitute.For<IEmailGateway>();
            transfer = new Transfer(emailGateway);
            fromAccount = new Account(1.0M);
            fromAccount.EmailAddress = "test";
            toAccount = new Account(1.0M);
        }

        [Test]
        public void TransferFunds_FromAccountToAccount_CorrectBalanceOnBothAccounts() {
            //Given
            decimal amountToTransfer = 0.5M;
            decimal expectedFromAccountBalance = 0.5M;
            decimal expectedToAccountBalance = 1.5M;

            //When
            transfer.TransferFunds(fromAccount, toAccount, amountToTransfer);

            //Then
            Assert.That(fromAccount.Balance, Is.EqualTo(expectedFromAccountBalance));
            Assert.That(toAccount.Balance, Is.EqualTo(expectedToAccountBalance));
            emailGateway.Received(1).SendEmail("Funds transferred", "bdd@bank.com", "test", $"Successfully transfered funds in the amoumt of: {amountToTransfer}");

        }

        [Test]
        public void TransferFunds_FromAccountToAccountWithOverflowAmount_ThrowException() {
            decimal amountToTransfer = 1.1M;

            InvalidOperationException exception = Assert.Throws<InvalidOperationException>(() =>
                transfer.TransferFunds(fromAccount, toAccount, amountToTransfer));
            Assert.IsNotNull(exception);

            Assert.That(() => transfer.TransferFunds(fromAccount, toAccount, amountToTransfer), Throws.TypeOf<InvalidOperationException>().With.Message.Contains("1,1"));

            Assert.That(() => transfer.TransferFunds(fromAccount, toAccount, amountToTransfer),
                Throws.TypeOf<InvalidOperationException>()
                    .With.Message.EqualTo($"Cannot transfer {amountToTransfer} from this account because the current balance is only {fromAccount.Balance}"));
        }
    }
}
