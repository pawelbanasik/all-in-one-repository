﻿namespace Visitor {
    public class IncomeVisitor : IVisitor {
        public double Amount { get; set; }

        public void Visit(RealEstate realEstate) {
            Amount += realEstate.MonthlyRent;
        }

        public void Visit(BankAccount bankAccount) {
            Amount += bankAccount.Amount * bankAccount.MonthlyInterest;
        }

        public void Visit(Loan loan) {
            Amount -= loan.MonthlyPayment;
        }
    }
}
