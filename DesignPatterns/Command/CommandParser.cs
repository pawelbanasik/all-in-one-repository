﻿using System.Collections.Generic;
using System.Linq;

namespace Command {
    public class CommandParser {
        private readonly IEnumerable<ICommandFactory> _availableCommands;

        public CommandParser(IEnumerable<ICommandFactory> availableCommands) {
            _availableCommands = availableCommands;
        }

        internal ICommand ParseCommand(string[] args) {
            string requestedCommandName = args[0];
            ICommandFactory command = FindRequestedCommand(requestedCommandName);
            if (command == null) {
                return new NotFoundCommand { Name = requestedCommandName };
            }
            return command.MakeCommand(args);
        }

        ICommandFactory FindRequestedCommand(string commandName) {
            return _availableCommands
                .FirstOrDefault(cmd => cmd.Name == commandName);
        }
    }
}