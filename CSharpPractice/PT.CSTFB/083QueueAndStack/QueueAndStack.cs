﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp._083QueueAndStack {

    public class Customer {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Gender { get; set; }

        public Customer(int id, string name, string gender) {
            Id = id;
            Name = name;
            Gender = gender;
        }

        public override bool Equals(object obj) {
            var customer = obj as Customer;
            return customer != null &&
                   Id == customer.Id &&
                   Name == customer.Name &&
                   Gender == customer.Gender;
        }

        public override int GetHashCode() {
            var hashCode = -1386332522;
            hashCode = hashCode * -1521134295 + Id.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Name);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Gender);
            return hashCode;
        }

        public override string ToString() {
            StringBuilder sb = new StringBuilder();
            sb.Append($"Id: {Id}, ");
            sb.Append($"Name: {Name}, ");
            sb.Append($"Gender: {Gender}");
            return sb.ToString();

        }
    }

    class QueueAndStack {
        static void MainQueueAndStack(string[] args) {

            Customer customer1 = new Customer(101, "Mark", "Male");
            Customer customer2 = new Customer(102, "Pam", "Female");
            Customer customer3 = new Customer(103, "John", "Male");
            Customer customer4 = new Customer(104, "Ken", "Male");
            Customer customer5 = new Customer(105, "Valerie", "Female");

            // 1. Queue
            Queue<Customer> queueCustomers = new Queue<Customer>();
            queueCustomers.Enqueue(customer1);
            queueCustomers.Enqueue(customer2);
            queueCustomers.Enqueue(customer3);
            queueCustomers.Enqueue(customer4);
            queueCustomers.Enqueue(customer5);

            // look what is first
            Console.WriteLine(queueCustomers.Peek());

            // remove the first one from the queue
            queueCustomers.Dequeue();

            // iterate
            foreach (Customer customer in queueCustomers) {
                Console.WriteLine(customer);
            }

            // 2. Stack
            Stack<Customer> stackCustomers = new Stack<Customer>();
            stackCustomers.Push(customer1);
            stackCustomers.Push(customer2);
            stackCustomers.Push(customer3);
            stackCustomers.Push(customer4);
            stackCustomers.Push(customer5);

            // remove and return
            stackCustomers.Pop();

            // return item without removing
            Console.WriteLine(stackCustomers.Peek());



        }
    }
}
