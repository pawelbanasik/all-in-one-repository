﻿using System;

namespace ConsoleApp._039DelegateMulticast {

    public delegate void SampleDelegate();

    public class InitilizerMulticast {
        public static void Init() {
            SampleDelegate sd = new SampleDelegate(SampleMethodOne);
            sd += SampleMethodTwo;
            sd += SampleMethodThree;
            sd();

            Action action = SampleMethodOne;
            action += SampleMethodTwo;
            action += SampleMethodTwo;
            action += SampleMethodThree;
            action();

        }

        public static void SampleMethodOne() {
            Console.WriteLine($"SampleMethodOne invoked");
        }

        public static void SampleMethodTwo() {
            Console.WriteLine($"SampleMethodTwo invoked");
        }

        public static void SampleMethodThree() {
            Console.WriteLine($"SampleMethodThree invoked");
        }
    }
}
