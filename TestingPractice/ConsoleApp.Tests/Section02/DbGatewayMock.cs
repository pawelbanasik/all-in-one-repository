﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleApp.Section02;

namespace ConsoleApp.Tests.Section02 {
    class DbGatewayMock : IDbGateway {
        public int Id { get; set; }
        private WorkingStatistics ws;

        public DbGatewayMock(WorkingStatistics ws) {
            this.ws = ws;
        }

        public WorkingStatistics GetWorkingStatistics(int id) {
            Id = id;
            return ws;
        }

        public bool Connected { get; }

        public bool VerifyCalledWithProperId(int id) {
            return Id == id;
        }

    }
}
