﻿using System;

namespace BuilderWithState {
    public class PersonBuilder {
        private INonEmptyStringState FirstNameState { get; set; } = new UninitializedString();
        private INonEmptyStringState LastNameState { get; set; } = new UninitializedString();

        public void SetFirstName(string firstName) {
            FirstNameState = FirstNameState.Set(firstName);
        }

        public void SetLastName(string lastName) {
            LastNameState = LastNameState.Set(lastName);
        }

        public Person Build() {
            return new Person(FirstNameState.Get(), LastNameState.Get());
        }

    }
}
