﻿using System;

// First type:
// 1. dummy - returns null or 0 from methods
// 2. stub - returns predefined constants from methods

// Second type (interaction testing):
// 3. spy - saves more information about the 
// calls (verify arguments, how many times method called)
// 4. mock (it is a spy) - but also holds assertions
// inside himself (mock knows what should happen when 
// inputing certain values)
// 5. fake - as close as possible to real dependency
namespace ConsoleApp {
    class Program {
        static void Main(string[] args) {
            Console.WriteLine("Hello world!");



        }
    }
}
