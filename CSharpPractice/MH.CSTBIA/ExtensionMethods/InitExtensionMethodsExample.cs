﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleApp.ExtensionMethodsExample {
    public class InitExtensionMethodsExample {
        public void Init() {
            string post = "This is suppose to be a very long blog post blah blah blah ...";
            var shortenedPost = post.Shorten(5);
            Console.WriteLine(shortenedPost);

            IEnumerable<int> numbers = new List<int> {
                1,3,4,5,6
            };
            Console.WriteLine(numbers.Max());
        }
    }
}