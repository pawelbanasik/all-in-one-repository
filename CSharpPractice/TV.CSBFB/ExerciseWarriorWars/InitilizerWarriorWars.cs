﻿using System;
using System.Threading;

namespace ConsoleApp.TV_WarriorWarsExercise {
    public class InitilizerWarriorWars {
        public void Init() {
            var warriorBad = new Warrior("Evil Guy", Weapon.Knife);
            var warriorGood = new Warrior("Good Guy", Weapon.Gun);

            while (true) {
                warriorBad.AttackOtherWarrior(warriorGood);
                if (warriorGood.IsDead()) {
                    AnnounceWinner(warriorBad);
                    break;
                }

                Thread.Sleep(2000);

                warriorGood.AttackOtherWarrior(warriorBad);
                if (warriorBad.IsDead()) {
                    AnnounceWinner(warriorGood);
                    break;
                }
            }

        }
        private static void AnnounceWinner(Warrior warrior) {
            Tools.ColorfullWriteLine($"Warrior {warrior.Name} has won.", ConsoleColor.Red);
        }
    }
}