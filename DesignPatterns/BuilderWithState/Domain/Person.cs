﻿using System;

namespace BuilderWithState {
    public class Person {
        public string Name { get; }
        public string Surname { get; }

        public Person(string name, string surname) {
            if (string.IsNullOrEmpty(name)) {
                throw new ArgumentException();
            }

            if (string.IsNullOrEmpty(surname)) {
                throw new ArgumentException();
            }

            Name = name;
            Surname = surname;
        }

        public override string ToString() => $"{Name} {Surname}";

    }
}
