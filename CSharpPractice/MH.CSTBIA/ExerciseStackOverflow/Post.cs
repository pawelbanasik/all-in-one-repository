﻿using System;

namespace ConsoleApp.StackOverflowExercise {
    public class Post {
        public string Title { get; }
        public string Description { get; }
        public DateTime CreationDateTime { get; }
        public int VoteValue { get; set; }

        public Post(string title, string description, DateTime creationDateTime) {
            Title = title;
            Description = description;
            CreationDateTime = creationDateTime;
        }

        public void UpVote() {
            VoteValue++;
            PrintCurrentVoteValue();
        }

        public void DownVote() {
            VoteValue--;
            PrintCurrentVoteValue();
        }

        public void PrintCurrentVoteValue() {
            Console.WriteLine(CreationDateTime + ":" + "Current vote value on the post with title " + Title + " is: " + VoteValue);
        }

    }
}