﻿namespace Strategy {
        public enum ShippingOptions {
            Ups = 100,
            FedEx = 200,
            Usps = 300,
        };
}