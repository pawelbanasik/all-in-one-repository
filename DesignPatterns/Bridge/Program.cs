﻿using System;
using System.Collections.Generic;

namespace Bridge {

    class Program {
        static void Main(string[] args) {
            List<Manuscript> documents = new List<Manuscript>();

            IFormatter formatter = new BackwardsFormatter();

            FAQ faq = new FAQ(formatter);
            faq.Title = "The Bridge Pattern FAQ";
            faq.Questions.Add("What is it?", "A design pattern");
            faq.Questions.Add("When do we use it?", "When you need to separate an abstraction from an implementation.");
            documents.Add(faq);

            Book book = new Book(formatter) {
                Title = "Lots of Patterns",
                Author = "John Sonmes",
                Text = "Blah blah blah"
            };
            documents.Add(book);

            TermPaper paper = new TermPaper(formatter) {
                Class = "Design Patterns",
                Student = "Joe Noob",
                Text = "Blah blah",
                References = "GOF"
            };
            documents.Add(paper);

            foreach (var doc in documents) {
                doc.Print();
            }

            Console.ReadKey();
        }
    }
}
