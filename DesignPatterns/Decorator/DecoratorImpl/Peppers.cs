﻿namespace Decorator {
    public class Peppers : PizzaDecorator {
        public Peppers(Pizza pizza) : base(pizza) {
            Description = "Peppers";
        }

        public override double CalculateCost() {
            return _pizza.CalculateCost() + 1.35;
        }

        public override string GetDescription() {
            return $"Description: {_pizza.GetDescription()}, {Description}";
        }
    }
}
