#if INTERACTIVE
#else
module JumpStart
#endif

// w let zapisujemy zmienne i funkcje
let x = 42
let hi = "Hello"

let Square x = x * x

// bez narzucania
// nesting przy pomocy formatowania
let Area length height = 
    length * height

// z narzuceniem typu w tych nawiasach oraz narzuecenie typu zwracanego na koncu
let Area2 (length:float) (height:float) : float=
    length * height

// if
let Greeting name =
    if System.String.IsNullOrWhiteSpace(name) then
        "whoever you are"
    else
        name

// syntax when getting return to another function
// nawiasy daje sie po to zeby funkcja nie myslala ze dostaje 3 argumenty
let SayHiTo me = 
    printfn "Hi, %s" (Greeting me)

// for each
let PrintNumbers min max =
    for x in min..max do
        printfn "%i" x
        
// mozna dac funkcje w funkcji i ja wykorzystac
let PrintNumbers2 min max =
    let square n =
        n * n
    for x in min..max do
        printfn "%i -> %i" x (square x)

// latwo zrobic tupla
let RandomPosition() =
    let r = new System.Random()
    r.NextDouble(), r.NextDouble()

// tak mozna rozbic tupla
let test1, test2 = RandomPosition()

// tupling arguments
let Sample (length, height) =
    length * height

open System.IO
open System
open System.Drawing

let PrintFiles() =
    let files = Directory.EnumerateFiles(@"c:\windows","*.exe")
    for f in files do
    printf "%s \n" f

// Arrays (musza byc sredniki bo inaczej stworzy sie tuple) - array elements are mutable
let array = [|1;2;3|]

//zeby opuscic sredniki w array'u
let arrayTwo = [|
    "apple"
    "orange"
    "pear"
    |]

let PrintValues() array=
    for v in array do
            printfn "%s" (v.ToString())

let numbers = [|1..30|]

let squares = [| for i in 0..99 do yield i * i|]

// tworzy array owocow z innego arraya bierze randomowo i wielkosc taka  jak count
let RandomFruits count =
    let r = System.Random()
    let fruits = [|"apple";"orange";"pear"|]
    [|
    for i in 1..count do
        let index = r.Next(3)
        yield fruits.[index]
    |]

// to samo tylko funkcja Array.init gdzie dajesz wielkosc i funkcje generujaca
// zeby odczytac index z danego array trzeba dac kropke do indexera
let RandomFruits2 count = 
    let r = System.Random()
    let fruits = [|"apple";"orange";"pear"|]
    Array.init count (fun _ ->
    let index = r.Next(3)
    fruits.[index]
    )

// iterowanie 1
let LikeSomeFruit fruits =
    for fruit in fruits do
        printfn "I like %s" fruit

 // iterowanie 2
let SaySomeNumbers() =
    for i in 0..9 do
        printfn "Number: %i" i

// filtering arrays
let squares2 = [|
    for i in 0..99 do
        yield i * i
    |]

// tu widac ze dla predykatow jest pojedynczy znak =
let IsEven number =
    number % 2 = 0

 // dajemy funkcje i dajemy lambde tam
let evenSquares = Array.filter(fun x -> IsEven x) squares2

let sortedFruit = Array.sort[|"pear"; "orange"; "apple"|]

// without pipe
let PrintLongWords (words : string[]) =
    let longWords : string[] = Array.filter(fun w -> w.Length > 5) words
    let sortedWords : string[] = Array.sort(longWords)
    Array.iter(fun w -> printf "%s\n" w) sortedWords
    //for w in sortedWords do
        //printf "%s\n" w

// with pipe
let PrintLongWords2 (words: string[]) =
    words
    |> Array.filter(fun w -> w.Length > 5)
    |> Array.sort
    |> Array.iter(fun w -> printf "%s\n" w)

// array map (same as select)
let PrintSquaresWithMap min max =
    let square i =
        i * i
    [|min..max|]
    |> Array.map(fun e -> square e)
    |> Array.iter(fun e -> printfn "%i" e)
        
let PrintArray() = 
    let numbers = [|10..20|]
    numbers.[0] <- 99
    numbers
    |> Array.iter(fun e -> printfn "%i" e)

// List - list elements are immutable
let listOfNumbes = 
    [1..20]

// Lists module for map, iter etc
let PrintSquaresList min max =
    let square n = 
        n * n
    [min..max]
    |> List.map(fun e -> square e)
    |> List.iter(printf "%i")

//Sequences
// 1 way to create
let numbersSequence = {0..99}

//2 way to create
let numbersSequence2 = Seq.init 100 (fun i -> i)

//3 way to create
let numbersSequence3 = 
    seq {
    for i in 0..99 do
        yield i
    }

let bigFiles =
    Directory.EnumerateFiles(@"c:\windows")
    |> Seq.map (fun name -> FileInfo name)
    |> Seq.filter (fun fi -> fi.Length > 1000000L)
    |> Seq.map (fun fi -> fi.Name)

// Record (to jest taka wlasnie konstrukcja)
type Person = {
FirstName : string
LastName : string
}

//Instantiate Record
let person = {FirstName = "Pawel"; LastName = "Banasik"}

let PrintPerson() = 
    person |> (fun p -> printfn "%s, %s" p.FirstName p.LastName)

// "with" statement
let person2 = {person with FirstName = "Michal"}

//equality
let person3 = {FirstName = "Pawel"; LastName = "Banasik"}

let EqualityTest() = 
    person = person3 |> fun e -> printfn "%s" (e.ToString())

//Option types - helps with null reference excetpion (taki optional)
type Company = {
    Name : string
    SalesTaxNumber : int option
}

let company = {Name ="Test Company"; SalesTaxNumber = None}

let company2 = {Name = "Test Company 2"; SalesTaxNumber = Some 12345}

// print kiedy mamy i nie mamy SalesTaxNumber (taki smieszny if optional - mozna zrobic to ifem ale wtedy
// trzeba value wyciagac) - to pomaga nam uniknac null reference jak uzywamy option
let PrintCompany (company: Company) =
    let taxNumberString = 
        match company.SalesTaxNumber with
        | Some n -> sprintf "[%i]" n
        | None -> ""

    printfn "%s %s" company.Name taxNumberString

// Discriminated unions (we dont need and object hierarchy but just a DU) - this float represents the size
// of the shape in this scenario
// to jest taka wlasnie konstrukcja to jest DU
type Shape = 
    | Square of float
    | Rectangle of float * float
    | Circle of float

let s = Square 3.4
let r = Rectangle (2.2, 1.9)
let c = Circle 1.0

// putting all of them into a same array - type is Shape[] in tooltip
let drawing = [|s; r; c|]

// match to takie cos jak switch na sterydach, jak nie pokryjesz przypadku to masz podpowiedz!
let AreaWithDiscriminatedUnions (shape : Shape) =
    match shape with
    | Square x -> x * x
    | Rectangle (h,w) -> h * w
    | Circle r -> System.Math.PI * r * r

let total = drawing |> Array.sumBy(fun s -> AreaWithDiscriminatedUnions s)

let one = [|50|]
let two = [|60; 61|]
let many = [|0..99|]

// zeby wywolac to starczy wpisac Describe one i masz info wydrukowane
let Describe array = 
    match array with
    | [|x|] -> sprintf "One element: %i" x
    | [|x; y|] -> sprintf "Two elements: %i, %i" x y
    | _ -> sprintf "A longer array"

//immutability
let ImmutabilityDemo() =
    let x = 42
    //x <- 43 doesn't work
    let x = 42
    let x = 43
    x

//wartosc nie zmieni sie niestety
let ImmutabilityDemo2() =
    let x =42
    printfn "x: %i" x
    if 1 = 1 then
        let x = x + 1
        printfn "x: %i" x
    printfn "x: %i" x

//overcoming immutability
let ImmutabilityDemo3() =
    let x = 42
    let mutable x = 43
    x

//let mutability demo
let MutabilityDemo() =
    let mutable x = 42
    printfn "x: %i" x
    x <- x + 1
    printfn "x: %i" x

// ref cells - to workaround immutability (often used in sequence generator)
let RefCellDemo() =
    let x = ref 42
    x := 43
    printfn "x: %i" !x

// often used to sequence generator (better write code immutable and don't use this crap)
let PrintLines() =
    seq {
        let finished = ref false
        while not !finished do
            match System.Console.ReadLine() with
            | null -> finished := true
            | s -> yield s
    }

