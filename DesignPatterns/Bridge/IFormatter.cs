﻿namespace Bridge {
    public interface IFormatter {
        string Format(string value);
    }
}
