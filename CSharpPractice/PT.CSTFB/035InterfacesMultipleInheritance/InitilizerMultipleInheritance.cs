﻿using System;

namespace ConsoleApp._035InterfacesMultipleInheritance {
    public interface IA {
        void AMethod();
    }

    public interface IB {
        void BMethod();
    }

    public class A : IA {
        public void AMethod() {
            Console.WriteLine("A");
        }
    }

    public class B : IB {
        public void BMethod() {
            Console.WriteLine("B");
        }
    }

    public class AB : IA, IB {
        A a = new A();
        B b = new B();
        public void AMethod() {
            a.AMethod();
        }

        public void BMethod() {
            b.BMethod();
        }
    }

    class InitilizerMultipleInheritance {
        public void Init() {
            AB ab = new AB();
            ab.AMethod();
            ab.BMethod();
        }

    }

}
