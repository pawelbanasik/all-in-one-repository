﻿namespace LazyLoad {
    public class Customer {
        public string Address { get; set; } = "Default Address.";
        public string CompanyName { get; set; } = "Default Company Name.";
    }
}