﻿namespace Bridge {
    public class StandardFormatter : IFormatter {
        public string Format(string value) {
            return string.Format($"{value}");
        }
    }
}
