﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleApp.StackExercise {
    public class Stack {
        private List<object> list = new List<object>();

        public void Push(object obj) {
            if (obj == null) {
                throw new InvalidOperationException("You cannot add null to the stack");
            }
            list.Add(obj);
        }

        public object Pop() {
            if (list.Count == 0) {
                throw new InvalidOperationException("List is empty.");
            }

            object obj = list.Last();
            list.Remove(obj);
            return obj;
        }

        public void Clear() {
            list.Clear();
        }
    }
}