﻿using System;

namespace ConsoleApp._030Interfaces {
    public class Customer : ICustomer {
        public int Id { get; set; }
        public string Name { get; set; }
        public void Print() {
            Console.WriteLine($"I'm a {GetType().FullName}");
        }

        public void Run(string where) {
            Console.WriteLine($"I run to {where}");
        }
    }
}