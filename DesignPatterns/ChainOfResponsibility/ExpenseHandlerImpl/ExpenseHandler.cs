﻿using System;

namespace ChainOfResponsibility {
    public class ExpenseHandler : IExpenseHandler {
        public IExpenseApprover Approver { get; set; }
        public IExpenseHandler Next { get; set; }

        public ExpenseHandler(IExpenseApprover approver) {
            Approver = approver;
        }

        public ApprovalResponse Approve(IExpenseReport expenseReport) {
            ApprovalResponse response = Approver.ApproveExpense(expenseReport);
            if (response == ApprovalResponse.BeyondApprovalLimit) {
                if (Next != null) {
                    return Next.Approve(expenseReport);
                } else {
                    Console.WriteLine("Nobody can approve such a high amount.");
                }
            }
            return response;
        }

        public void RegisterNext(IExpenseHandler next) {
            Next = next;
        }
    }
}
