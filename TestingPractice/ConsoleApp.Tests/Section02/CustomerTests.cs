﻿using ConsoleApp.Section02;
using NSubstitute;
using NSubstitute.ExceptionExtensions;
using NUnit.Framework;
using System;

namespace ConsoleApp.Tests.Section02 {

    [TestFixture]
    class CustomerManualTests {

        // PART 1 - Manual without Mocking Framework

        [Test]
        public void CalculateWage_HourlyPayed_ReturnsCorrectWage() {
            DbGatewayStub dbGatewayStub = new DbGatewayStub();
            WorkingStatistics ws = new WorkingStatistics();
            ws.PayHourly = true;
            ws.HourSalary = 100;
            ws.WorkingHours = 10;
            dbGatewayStub.SetWorkingStatistics(ws);

            Customer customer = new Customer(dbGatewayStub, new LoggerDummy());
            int anyId = 1;
            decimal actual = customer.CalculateWage(anyId);

            const decimal expectedWage = 100 * 10;

            Assert.That(actual, Is.EqualTo(expectedWage).Within(0.1));

        }

        // 1st way with spy
        [Test]
        public void CalculateWage_PassesCorrectId() {
            int id = 1;
            DbGatewaySpy dbGatewaySpy = new DbGatewaySpy();
            dbGatewaySpy.SetWorkingStatistics(new WorkingStatistics());
            Customer customer = new Customer(dbGatewaySpy, new LoggerDummy());
            customer.CalculateWage(id);

            int expectedId = 1;

            Assert.That(dbGatewaySpy.Id, Is.EqualTo(expectedId));
        }

        // 2nd way with mock
        [Test]
        public void CalculateWage_PassesCorrectIdUsingMock() {
            int id = 13;
            WorkingStatistics ws = new WorkingStatistics();
            DbGatewayMock dbGatewayMock = new DbGatewayMock(ws);
            Customer customer = new Customer(dbGatewayMock, new LoggerDummy());
            customer.CalculateWage(13);


            // Assert.That(dbGatewayMock.VerifyCalledWithProperId(id), Is.EqualTo(true));
            Assert.IsTrue(dbGatewayMock.VerifyCalledWithProperId(id));
        }

        // I didn't write a fake unit test
        // fake example is almost the same but they are very hard to maintain



    }

    // PART 2 - Mocking Framework

    [TestFixture]
    public class CustomerClassWithMockingFramework {

        [Test]
        public void CalculateWage_HourlyPayed_ReturnsCorrectWage() {
            IDbGateway gatewayMock = Substitute.For<IDbGateway>();
            ILogger loggerDummy = Substitute.For<ILogger>();
            WorkingStatistics workingStatistics = new WorkingStatistics();
            workingStatistics.PayHourly = true;
            workingStatistics.HourSalary = 100;
            workingStatistics.WorkingHours = 10;

            //            int anyId = 13;
            int anyId = Arg.Any<int>();

            gatewayMock.GetWorkingStatistics(anyId).ReturnsForAnyArgs(workingStatistics);
            gatewayMock.Connected.Returns(true);
            Customer customer = new Customer(gatewayMock, loggerDummy);

            decimal actualWage = customer.CalculateWage(anyId);

            decimal expectedWage = 100 * 10;

            Assert.That(actualWage, Is.EqualTo(expectedWage).Within(0.1));
        }

        // mocking frameworks don't have spies
        [Test]
        public void CalculateWage_PassesCorrectId() {
            IDbGateway gatewayMock = Substitute.For<IDbGateway>();
            int id = 13;
            WorkingStatistics emptyWorkingStatistics = new WorkingStatistics();
            gatewayMock.GetWorkingStatistics(id).ReturnsForAnyArgs(emptyWorkingStatistics);
            ILogger loggerDummy = Substitute.For<ILogger>();

            Customer customer = new Customer(gatewayMock, loggerDummy);
            customer.CalculateWage(id);

            int expectedId = 13;

            // way to check passed values using NSubstitue
            gatewayMock.Received().GetWorkingStatistics(expectedId);
        }

        [Test]
        public void CalculateWage_ThrowsException_Returns0() {
            var gatewayMock = Substitute.For<IDbGateway>();
            gatewayMock.GetWorkingStatistics(Arg.Any<int>()).Throws(new InvalidOperationException());
            ILogger loggerDummy = Substitute.For<ILogger>();
            Customer customer = new Customer(gatewayMock, loggerDummy);

            decimal actual = customer.CalculateWage(Arg.Any<int>());

            int expected = 0;

            Assert.That(actual, Is.EqualTo(expected));
        }
    }
}