﻿namespace BuilderWithFluentInterface {
    public interface ISecondaryContactHolder {
        ISecondaryContactHolder WithSecondaryContact(IContactInfo contact);
        IPersonBuilder AndNoMoreContacts();
    }
}
