﻿namespace AbstractFactory {
    class MacAddress : IUserIdentity {
        public string RawAddress { get; set; }
    }
}
