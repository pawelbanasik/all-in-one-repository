﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleApp.Section02;

namespace ConsoleApp.Tests.Section02 {
    class DbGatewayFake : IDbGateway {

        private Dictionary<int, WorkingStatistics> storage = new Dictionary<int, WorkingStatistics>() {
            { 1, new WorkingStatistics() {PayHourly = true, WorkingHours = 10, HourSalary = 10}},
            { 2, new WorkingStatistics() {PayHourly = false, WorkingHours = 3, HourSalary = 500}},
            { 3, new WorkingStatistics() {PayHourly = true, WorkingHours = 8, HourSalary = 100}},
        };



        public WorkingStatistics GetWorkingStatistics(int id) {
            return storage[id];

        }

        public bool Connected { get; }
    }
}
