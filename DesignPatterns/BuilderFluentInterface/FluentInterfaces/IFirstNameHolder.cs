﻿namespace BuilderWithFluentInterface {
    public interface IFirstNameHolder {
        ILastNameHolder WithFirstName(string name);
    }
}
