﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp._079Sorting {
    public class Customer : IComparable<Customer> {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Salary { get; set; }

        public Customer(int id, string name, int salary) {
            Id = id;
            Name = name;
            Salary = salary;
        }

        public override string ToString() {
            StringBuilder sb = new StringBuilder();
            sb.Append($"Id: {Id}, ");
            sb.Append($"Name: {Name}, ");
            sb.Append($"Salary: {Salary}, ");
            return sb.ToString();
        }

        // only 1 option of sorting
        public int CompareTo(Customer other) {
            return Salary.CompareTo(other.Salary);
        }
    }

    public class SortByName : IComparer<Customer> {
        public int Compare(Customer x, Customer y) {
            return x.Name.CompareTo(y.Name);

        }
    }

    class Program {
        static void MainForSorting(string[] args) {

            List<int> numbers = new List<int> { 1, 8, 7, 5, 2, 3, 4, 9, 6 };
            numbers.Sort();
            numbers.Reverse();
            numbers.ForEach(n => Console.WriteLine(n));

            List<string> alphabets = new List<string>() { "B", "F", "D", "E", "A", "C" };
            alphabets.Sort();
            alphabets.ForEach(a => Console.WriteLine(a));
            alphabets.Reverse();

            List<Customer> customers = new List<Customer>();
            Customer customer1 = new Customer(101, "Mark", 4000);
            Customer customer2 = new Customer(102, "Pam", 7000);
            Customer customer3 = new Customer(103, "Rob", 5500);
            customers.Add(customer1);
            customers.Add(customer2);
            customers.Add(customer3);
            customers.ForEach(c => Console.WriteLine(c));
            Console.WriteLine("---------------");

            // Cusomer sort - only one impl IComparable<Cusomer>
            customers.Sort();
            customers.ForEach(c => Console.WriteLine(c));

            // Another Customer sort of complex types by name - IComparer<Customer> 
            Console.WriteLine("-----------------");
            customers.Sort(new SortByName());
            customers.Sort();
            customers.ForEach(c => Console.WriteLine(c));

            Comparison<Customer> byId = (c1, c2) => c1.Id.CompareTo(c2.Id);
            customers.Sort(byId);
            customers.ForEach(c=> Console.WriteLine(c));
        }
    }
}
