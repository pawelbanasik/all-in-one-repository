﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PT.CSTFB._098AnonymousVsLambdaAndFunc {

    public class Employee {
        public int Id { get; set; }
        public string Name { get; set; }

        public Employee(int id, string name) {
            Id = id;
            Name = name;
        }

        public override bool Equals(object obj) {
            var employee = obj as Employee;
            return employee != null &&
                   Id == employee.Id &&
                   Name == employee.Name;
        }

        public override int GetHashCode() {
            var hashCode = -1919740922;
            hashCode = hashCode * -1521134295 + Id.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Name);
            return hashCode;
        }

        public override string ToString() {
            StringBuilder sb = new StringBuilder();
            sb.Append($"Id: {Id}, ");
            sb.Append($"Name: {Name}");
            return sb.ToString();
        }
    }

    class Func {
        static void MainFunc(string[] args) {
            List<Employee> employees = new List<Employee>() {
                new Employee(101, "Mark"),
                new Employee(102, "John"),
                new Employee(103, "Mary")
            };

            // anonymous function - C# 2.0 way
            Employee employee = employees.Find(delegate (Employee e) { return e.Id == 102; });
            Console.WriteLine(employee);

            // lambda expression - C# 3.0 better way
            Employee find = employees.Find(c => c.Id == 102);
            Console.WriteLine(find);

            Action<int> justPrint = i => Console.WriteLine(i);
            Predicate<int> predicatePrintNumbers = i => i < 10;
            Func<int, string> funcStringConvert = i => i.ToString();

            justPrint(10);
            Console.WriteLine(predicatePrintNumbers(9));
            Console.WriteLine(funcStringConvert(14));

            Func<Employee, string> selector = e => $"Name {e.Name}";
            employees.ForEach(e => Console.WriteLine(selector(e)));

        }



    }
}
