﻿namespace ConsoleApp._030Interfaces {
    public interface ICustomer : IRun {
        int Id { get; set; }
        string Name { get; set; }
        void Print();
    }
}