﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Decorator {
    class Program {
        static void Main(string[] args) {
            Pizza largePizza = new LargePizza();
            largePizza = new Cheese(largePizza);
            largePizza = new Ham(largePizza);
            largePizza = new Peppers(largePizza);
            Console.WriteLine(largePizza.GetDescription());
            Console.WriteLine($"{largePizza.CalculateCost()}");
        }
    }
}
