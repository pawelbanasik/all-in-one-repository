﻿namespace Mediator {
    public class Airbus321 : Aircraft {
        public Airbus321(IAirTrafficControl airTrafficControl, string callSign) : base(airTrafficControl, callSign) {
        }

        public override int Ceiling => 41000;
    }
}
