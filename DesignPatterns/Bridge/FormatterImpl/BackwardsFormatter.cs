﻿using System;
using System.Linq;

namespace Bridge {
    public class BackwardsFormatter : IFormatter {
        public string Format(string value) {
            return string.Format($"{new String(value.Reverse().ToArray())}");
        }
    }
}
