﻿using System;

namespace Command {
    public class CreateOrderCommand : ICommand, ICommandFactory {
        public string Name { get { return "CreateOrder"; } }
        public string Description { get { return Name; } }

        public ICommand MakeCommand(string[] arguments) {
            throw new NotImplementedException();
        }

        public void Execute() {
            throw new NotImplementedException();
        }

    }
}