﻿namespace ConsoleApp._030Interfaces {
    public class Initilizer {
        public static void Init() {
            ICustomer customer = new Customer();
            customer.Print();
            customer.Run("city");
        }
    }
}