﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LinqPractice._02Queries {
    public class Student {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Gender { get; set; }

        public Student(int id, string name, string gender) {
            Id = id;
            Name = name;
            Gender = gender;
        }

        public static List<Student> GetAllStudents() {
            Student student1 = new Student(101, "Mark", "Male");
            Student student2 = new Student(102, "Mary", "Female");
            Student student3 = new Student(103, "John", "Male");
            Student student4 = new Student(104, "Steve", "Male");
            Student student5 = new Student(105, "Pam", "Female");
            List<Student> students = new List<Student>() {
                student1, student2, student3, student4, student5
            };
            return students;
        }
    }

    public class StringHelper {
        public static string ChangeFirstLetterCase(string input) {
            if (input.Length > 0) {
                char[] array = input.ToCharArray();
                array[0] = char.IsUpper(array[0]) ? char.ToLower(array[0]) : char.ToUpper(array[0]);
                return new string(array);
            }
            return input;

        }


    }
    class Queries {
        static void Main02(string[] args) {

            List<Student> students = Student.GetAllStudents();

            // with fluent syntax
            List<Student> list = students.Where(s => s.Gender == "Male").ToList();

            // with sql like query
            IEnumerable<Student> enumerable = from student in students
                                              where student.Gender == "Male"
                                              select student;

            string text = "pawel";
            string result = StringHelper.ChangeFirstLetterCase(text);
            Console.WriteLine(result);
        }
    }
}
