﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace log4netInDepth {

    public class LogHelper {
        public static log4net.ILog GetLogger([CallerFilePath]string filename = "") {
            return log4net.LogManager.GetLogger(filename);
        }
    }

    class Program {

        // below .NET 4.6 - 2 options
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static readonly log4net.ILog log2 = log4net.LogManager.GetLogger("Program.cs");

        // new feature from .NET 4.6
        // using LogHelper class - this will give full path
        private static readonly log4net.ILog log3 = LogHelper.GetLogger();

        static void Main(string[] args) {
            Console.WriteLine("Hello world.");
            log.Debug("Debug.");
            log.Info("Info.");
            log.Warn("Warn.");
            log.Error("Error.");
            log.Fatal("Fatal.");

            log.Info("Using reflection");
            log2.Info("Using hardcoded class name");
            log3.Info("LogHelper log message");

            // logging exceptions in console
            int i = 0;

            try {
                int j = 10 / i;
            } catch (Exception e) {
                log.Error("Divide by zero!", e);
            }

            log.Error("Elephant");
        }
    }
}
