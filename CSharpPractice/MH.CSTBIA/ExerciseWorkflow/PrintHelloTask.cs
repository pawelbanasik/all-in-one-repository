﻿using System;

namespace ConsoleApp.WorkflowExercise {
    public class PrintHelloTask : IActivity {
        public void Execute() {
            Console.WriteLine("Hello.");
        }
    }
}