﻿using System.Collections.Generic;
using System.Linq;

namespace ConsoleApp._066Indexers {
    public class Company {
        public List<Employee> Employees { get; set; }

        public Company() {
            Employees = new List<Employee> {
                new Employee(1, "Pawel", "Male"),
                new Employee(2, "Grzegorz", "Male"),
                new Employee(3, "Michal", "Female"),
                new Employee(4, "Heniek", "Male"),
            };
        }

        public string this[int Id] {
            get { return Employees.FirstOrDefault(emp => emp.Id == Id).Name; }
            set { Employees.FirstOrDefault(emp => emp.Id == Id).Name = value; }
        }

        public string this[string gender] {
            get { return Employees.Count(employee => employee.Gender == gender).ToString(); }
            set {
                Employees.ForEach(emp => {
                    if (emp.Gender == gender) {
                        emp.Gender = value;
                    }
                });



            }
        }


    }
}