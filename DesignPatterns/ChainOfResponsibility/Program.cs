﻿using System;
using System.Collections.Generic;

namespace ChainOfResponsibility {
    class Program {
        static void Main(string[] args) {
            ExpenseHandler william = new ExpenseHandler(new Employee("William Worker", Decimal.Zero));
            ExpenseHandler mary = new ExpenseHandler(new Employee("Mary Manager", new Decimal(1000)));
            ExpenseHandler victor = new ExpenseHandler(new Employee("Victoria Vicepres", new Decimal(5000)));
            ExpenseHandler paula = new ExpenseHandler(new Employee("Paula President", new Decimal(20000)));

            william.RegisterNext(mary);
            mary.RegisterNext(victor);
            victor.RegisterNext(paula);

            Console.WriteLine("Input amount to approve:");
            decimal expenseReportAmount = decimal.Parse(Console.ReadLine());
            IExpenseReport expense = new ExpenseReport(expenseReportAmount);
            ApprovalResponse response = william.Approve(expense);
            Console.WriteLine($"The request was {response}");
        }
    }
}
