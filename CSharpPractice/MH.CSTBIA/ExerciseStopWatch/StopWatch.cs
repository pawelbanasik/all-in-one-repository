﻿using System;

namespace ConsoleApp.StopWatchExercise {
    public class StopWatch {
        private DateTime startTime;
        private DateTime endTime;
        private bool isRunning;

        public void Start() {
            if (isRunning) {
                throw new InvalidOperationException("StopWatch is already running!");
            }
            isRunning = true;
            Console.WriteLine("StopWatch started!");
            startTime = DateTime.Now;

        }

        public void Stop() {
            if (!isRunning) {
                throw new InvalidOperationException("StopWatch is already running!");
            }
            Console.WriteLine("StopWatch stopped!");
            endTime = DateTime.Now;
            TimeSpan duration = endTime - startTime;
            Console.WriteLine("Duration: " + duration);
            isRunning = false;
        }

    }
}