﻿using System;
using System.Collections.Generic;

namespace ConsoleApp._038DelegateUsage {
    public class Promoter {
        public static void PromoteByExp(List<Employee> employees) {
            employees
                .FindAll(e => e.Experience >= 5)
                .ForEach(e => Console.WriteLine($"{e.Name} promoted."));
        }
    }

    public class Employee {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Salary { get; set; }
        public int Experience { get; set; }

        public Employee(string name, int experience) {
            Name = name;
            Experience = experience;
        }

        public static void PromoteEmployee(List<Employee> employees, Action<List<Employee>> action) {
            action(employees);
        }

    }


    public class InitilizerProgram {
        public static void Init() {
            List<Employee> employees = new List<Employee>();
            employees.Add(new Employee("Pawel", 9));
            employees.Add(new Employee("Grzegorz", 12));
            Employee.PromoteEmployee(employees, Promoter.PromoteByExp);
        }
    }
}