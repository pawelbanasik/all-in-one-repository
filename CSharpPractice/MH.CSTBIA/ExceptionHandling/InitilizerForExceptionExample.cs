﻿using System;

namespace ConsoleApp.ExceptionHandling {
    public class InitilizerForExceptionExample {
        public void Init() {
            //            try {
            //                using (var sr = new StreamReader(@"c:\test.txt")) {
            //                    sr.ReadToEnd();
            //                }
            //            } catch (Exception ex) {
            //                Console.WriteLine(ex.Message);
            //            }

            try {
                var api = new YouTubeApi();
                var videos = api.GetVideos("Pawel");
            } catch (Exception e) {
                Console.WriteLine(e.Message);
                throw;
            }
        }
    }
}