﻿using ConsoleApp.Section01;
using Machine.Specifications;

namespace ConsoleApp.Spec.Section01 {
    class CalculatorSpec {
    }

    public abstract class CalculatorSpecsBase {
        protected static Calculator Calculator;

        private Establish context = () => {
            Calculator = new Calculator();
        };

    }

    [Subject("Adding numbers")]
    public class When_Adding_Two_Numbers : CalculatorSpecsBase {
//        Because of = () => Calculator.AddNumbers(1, 2);
        It Should_Equal_The_Expected_Sum = () => Calculator.AddNumbers(1, 2).ShouldEqual(3);

    }

}
