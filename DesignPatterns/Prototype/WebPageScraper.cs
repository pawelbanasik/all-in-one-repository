﻿using System;
using System.Net;

namespace Prototype {
    public class WebPageScraper : ICloneable {
        public string Title { get; set; }
        public int HeaderTagCount { get; set; }
        public string FirstHeaderTagCount { get; set; }

        public WebPageScraper(string url) {
            var client = new WebClient();
            Scrape(client.DownloadString(url));
            //Console.WriteLine(client.DownloadString(url));
        }

        public void Scrape(string page) {
            Title = "Fake title";
            HeaderTagCount = 3;
            FirstHeaderTagCount = "Fake header text";
        }

        public void PrintPageData() {
            Console.WriteLine($"Title: {Title}, Header count: {HeaderTagCount}, First header: {FirstHeaderTagCount}");
        }

        public object Clone() {
            return MemberwiseClone();
        }
    }
}
