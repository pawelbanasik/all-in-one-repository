﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LinqPractice._03ExtensionMethods {

    public static class StringHelper {
        public static string ChangeFirstLetterCase(this string input) {
            if (input.Length > 0) {
                char[] array = input.ToCharArray();
                array[0] = char.IsUpper(array[0]) ? char.ToLower(array[0]) : char.ToUpper(array[0]);
                return new string(array);
            }
            return input;

        }


    }
    class ExtensionMethods {
        static void Main03(string[] args) {
            string text = "pawel";
            //using extension method
            string result = text.ChangeFirstLetterCase();
            Console.WriteLine(result);

            List<int> numbers = new List<int>() { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            IEnumerable<int> enumerable = numbers.Where(i => i % 2 == 0);
            enumerable.ToList().ForEach(i => Console.WriteLine(i));
        }
    }
}
