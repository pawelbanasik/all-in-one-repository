﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using LibraryProject;

namespace ExecutionProject {
    class Program {
        static void Main(string[] args) {

            Assembly a = typeof(Program).Assembly;
            Console.WriteLine(a);
            Assembly.GetExecutingAssembly();
            Assembly.GetCallingAssembly();
            var aLocation = a.Location;
            Console.WriteLine(aLocation);
            var isInGAC = a.GlobalAssemblyCache;
            Console.WriteLine(isInGAC);
            var version = a.GetName().Version.ToString();
            Console.WriteLine(version);
        }
    }
}
