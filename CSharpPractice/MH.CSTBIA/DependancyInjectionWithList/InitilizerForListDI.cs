﻿namespace ConsoleApp.DependancyInjectionWithList {

    public class InitilizerForListDI {
        public void Init() {
            var encoder = new VideoEncoder();
            encoder.RegisterNotificationChannel(new MailNotificationChannel());
            encoder.RegisterNotificationChannel(new SmsNotificationChannel());
            encoder.Encode(new Video());
        }
    }
}