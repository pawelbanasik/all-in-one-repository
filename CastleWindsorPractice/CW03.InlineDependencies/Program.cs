﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Castle.Windsor.Installer;

namespace CW03.InlineDependencies {
    public interface IOrganization {
        string CompanyName { get; set; }
        string EmployeeCount { get; set; }

        string CorporateService();
        string ToString();
    }

    public class Organization : IOrganization {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public string CompanyName { get; set; }
        public string EmployeeCount { get; set; }
        private readonly IBussinessService bussinessService;

        public Organization() {
            log.Info("Default constructor is called.");
        }

        public Organization(IBussinessService bussinessService) {
            this.bussinessService = bussinessService;
        }

        public string CorporateService() {
            return bussinessService.OfferingService(CompanyName);
        }

        public override string ToString() {
            StringBuilder sb = new StringBuilder();
            sb.Append("Organization - ");
            sb.Append($"CompanyName: {CompanyName}, ");
            sb.Append($"EmployeeCount: {EmployeeCount}. ");
            return sb.ToString();
        }
    }

    public interface IBussinessService {
        string OfferingService(string companyName);
    }

    public class CloudServiceImpl : IBussinessService {
        public string OfferingService(string companyName) {
            const int revenue = 100_000_000;
            Random random = new Random(revenue);
            int randomRevenue = random.Next();
            string output = $"As an organization {companyName} offers world class Cloud computing infrastructure.\nThe annual income exceeds {randomRevenue} dollars.";
            return output;
        }
    }

    public class EcommerceServiceImpl : IBussinessService {
        public string OfferingService(string companyName) {
            const int revenue = 100_000_000;
            Random random = new Random(revenue);
            int randomRevenue = random.Next();
            string output = $"As an organization {companyName} offers world class Ecommerce platform.\nThe annual income exceeds {randomRevenue} dollars.";
            return output;
        }
    }

    public class MyInstaller : IWindsorInstaller {
        public void Install(IWindsorContainer container, IConfigurationStore store) {
            //            container.Register(
            //                Classes.FromThisAssembly()
            //                .Where(Component.IsInSameNamespaceAs<Organization>())
            //                .WithService.DefaultInterfaces()
            //                .LifestyleTransient());

            container.Register(
                Component.For<IOrganization>().ImplementedBy<Organization>()
                    .DependsOn(
                    Dependency.OnAppSettingsValue("CompanyName", "KCompanyName"),
                    Dependency.OnAppSettingsValue("EmployeeCount", "KEmployeeCount")
                    )
            );

            container.Register(
                Component.For<IBussinessService>()
                    .ImplementedBy<CloudServiceImpl>()
            );
        }
    }



    class Program {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        static void Main(string[] args) {
            IWindsorContainer container = new WindsorContainer();
            container.Install(FromAssembly.This());

            IOrganization organization = container.Resolve<IOrganization>();
            log.Info(organization);
            log.Info(organization.CorporateService());

            container.Dispose();
        }

    }
}
