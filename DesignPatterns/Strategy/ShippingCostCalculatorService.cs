﻿using System;

namespace Strategy {
    public class ShippingCostCalculatorService {
        //public IShippingCostStrategy IShippingCostStrategy { get; set; }

        //public ShippingCostCalculatorService(IShippingCostStrategy shippingCostStrategy) {
        //    IShippingCostStrategy = shippingCostStrategy;
        //}

        public ShippingCostCalculatorService() {
        }

        public double CalculateShippingCost(Order order, Func<Order, double> function) {
            return function(order);
        }
    }
}
