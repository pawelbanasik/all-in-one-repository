﻿using System;

namespace ConsoleApp.DependencyInjection {
    public class DbMigrator {
        private readonly ILogger logger;

        public DbMigrator(ILogger logger) {
            this.logger = logger;
        }

        public void Migrate() {
            logger.LogInfo("Migrating started at: " + DateTime.Now);
            logger.LogInfo("Migration finished at: " + DateTime.Now);
        }
    }
}
