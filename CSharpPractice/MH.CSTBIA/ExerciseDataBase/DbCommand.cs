﻿using System;
using System.Threading;

namespace ConsoleApp.MH_DataBaseExercise {
    public class DbCommand {
        public DbConnection DbConnection { get; set; }
        public string Instruction { get; set; }

        public DbCommand(DbConnection dbConnection, string instruction) {
            if (dbConnection == null) {
                throw new InvalidOperationException("Connection is null.");
            }
            DbConnection = dbConnection;

            if (String.IsNullOrWhiteSpace(instruction)) {
                throw new InvalidOperationException("Instruction string is null.");
            }
            Instruction = instruction;
        }

        public void Execute() {
            DbConnection.Open();
            Thread.Sleep(3000);
            Console.WriteLine("Making instructions: " + Instruction);
            Thread.Sleep(3000);
            DbConnection.Close();
        }
    }
}