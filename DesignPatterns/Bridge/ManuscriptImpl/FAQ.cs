﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Bridge {
    public class FAQ : Manuscript {
        public string Title { get; set; }
        public Dictionary<string, string> Questions { get; set; } = new Dictionary<string, string>();

        public FAQ(IFormatter formatter) : base(formatter) {
        }

        public override void Print() {
            Console.WriteLine($"Title {Title}");
            Questions.ToList().ForEach(q => {
                Console.WriteLine($"Question {q.Key}");
                Console.WriteLine($"Answer {q.Value}");
            });
            Console.WriteLine();
        }
    }
}
