﻿namespace Strategy {
    public interface IShippingCostStrategy {
        double Calculate(Order order);
    }
}
