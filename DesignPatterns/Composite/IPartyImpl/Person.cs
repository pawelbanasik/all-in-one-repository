﻿using System;

namespace Composite {
    public class Person : IParty {
        public string Name { get; set; }
        public int Gold { get; set; }

        public Person(string name) {
            Name = name;
        }

        public void Stats() {
            Console.WriteLine($"{Name} has {Gold} gold coins.");
        }
    }
}
