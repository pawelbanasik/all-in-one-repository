﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleApp.Section01;
using Machine.Specifications;

namespace ConsoleApp.Spec.Section01 {
    class SerialPortParserSpec {
    }

    public class SerialPortParserSpecBase {
    }

    public class When_SerialPortParser_Parses_Port_With_Proper_Value {
        private It Should_Return_Proper_Result = () => SerialPortParser.ParsePort("COM1").ShouldEqual(1);
    }

    public class When_SerialPortParser_Parse_Port_With_Bad_String_Input {
        private static Exception exception;
        private Because of = () => exception = Catch.Exception(() => SerialPortParser.ParsePort("1"));
        private It Should_throw_FormatException = () => exception.ShouldBeOfExactType<FormatException>();

    }

}
