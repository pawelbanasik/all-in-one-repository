﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Practice {
    public class Employee {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }

        public void PrintFullName() {
            Console.WriteLine(FirstName + LastName);
        }
    }

    public class FullTimeEmployee : Employee{
        public float YearlySalary { get; set; }
    }

    public class PartTimeEmployee : Employee {
        public float HourlyRate { get; set; }
    }

    class Program {
        static void Main(string[] args) {
            FullTimeEmployee fullTimeEmployee = new FullTimeEmployee();
            fullTimeEmployee.FirstName = "Pawel";
            fullTimeEmployee.LastName = "Banasik";
            fullTimeEmployee.PrintFullName();

            PartTimeEmployee partTimeEmployee = new PartTimeEmployee();
            partTimeEmployee.FirstName = "Michal";
            partTimeEmployee.LastName = "Banasik";

        }
    }
}