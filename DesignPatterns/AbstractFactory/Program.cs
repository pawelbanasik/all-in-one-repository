﻿using System;

namespace AbstractFactory {
    class Program {

        static void RegisterUser(IUserFactory userFactory) {
            IUser user = userFactory.CreateUser("Max", "Planck");
            Console.WriteLine($"Hello {user}, welcome back!");
        }

        static void Main(string[] args) {
            RegisterUser(new PersonFactory());
        }
    }
}
