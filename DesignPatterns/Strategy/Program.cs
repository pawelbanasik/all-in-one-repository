﻿using System;

namespace Strategy {
    class Program {
        static void Main(string[] args) {
            // 1. classic approach is commented out
            //var fedExStrategy = new FedExShippingCostStrategy();
            //var upsStrategy = new UPSShippingCostStrategy();
            //var uspsStrategy = new USPSShippingCostStrategy();

            // 2. using func and delegates is better
            Func<Order, double> fedExStrategy = order => 5.00d;
            Func<Order, double> upsStrategy = order => 4.00d;
            Func<Order, double> uspsStrategy = order => 3.25;

            Order theOrder = new Order();

            var calculatorService = new ShippingCostCalculatorService();
            //var calculatorService = new ShippingCostCalculatorService(fedExStrategy);
            //Console.WriteLine($"FedEx: {calculatorService.CalculateShippingCost(theOrder)}");
            Console.WriteLine($"FedEx: {calculatorService.CalculateShippingCost(theOrder, fedExStrategy)}");

            //calculatorService = new ShippingCostCalculatorService(upsStrategy);
            //Console.WriteLine($"Ups: {calculatorService.CalculateShippingCost(theOrder)}");
            Console.WriteLine($"Ups: {calculatorService.CalculateShippingCost(theOrder,upsStrategy)}");

            //calculatorService = new ShippingCostCalculatorService(uspsStrategy);
            Console.WriteLine($"Usps: {calculatorService.CalculateShippingCost(theOrder, uspsStrategy)}");
        }
    }
}