﻿using System;

namespace ConsoleApp.StackOverflowExercise {
    public class InitilizerStackOverflowExercise {
        public void Init() {
            var post = new Post("Hate storm!", "Fun post.", DateTime.Now);
            post.UpVote();
            post.UpVote();
            post.DownVote();
        }
    }
}