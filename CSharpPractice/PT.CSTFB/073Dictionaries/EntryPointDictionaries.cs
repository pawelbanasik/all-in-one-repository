﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApp._073Dictionaries {

    public class Customer {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Salary { get; set; }

        public Customer(int id, string name, int salary) {
            Id = id;
            Name = name;
            Salary = salary;
        }

        public override string ToString() {
            StringBuilder sb = new StringBuilder();
            sb.Append($"Id: {Id}, ");
            sb.Append($"Name: {Name}, ");
            sb.Append($"Salary: {Salary}");
            return sb.ToString();
        }

    }



    class EntryPointDictonaries {
        static void MainDictionaries(string[] args) {

            Customer customer1 = new Customer(101, "Mark", 5000);
            Customer customer2 = new Customer(110, "Pam", 6500);
            Customer customer3 = new Customer(119, "John", 3500);

            Dictionary<int, Customer> customers = new Dictionary<int, Customer>();
            customers.Add(customer1.Id, customer1);
            customers.Add(customer2.Id, customer2);
            customers.Add(customer3.Id, customer3);

            // get by key
            Customer customer119 = customers[119];

            // iterate
            customers.ToList().ForEach(keyValuePair => Console.WriteLine(keyValuePair));

            if (customers.ContainsKey(101)) {
                Console.WriteLine(customers[101]);
            }

            Customer customer;
            bool valueExists = customers.TryGetValue(101, out customer);
            if (valueExists) {
                Console.WriteLine($"{customer.Id} {customer.Name} {customer.Salary}");
            } else {
                Console.WriteLine("Key not found.");
            }

            int count = customers.Count(keyValuePair => keyValuePair.Value.Salary > 4000);
            Console.WriteLine(count);

            customers.Remove(101);
            customers.ToList().ForEach(keyValuePair => Console.WriteLine(keyValuePair));

            // array to dictionary
            Console.WriteLine("Array to dictionary:");
            Customer[] customersArray = { customer1, customer2, customer3 };
            Dictionary<int, Customer> dictionary = customersArray.ToDictionary(c => c.Id, c => c);
            dictionary.ToList().ForEach(keyValuePair => Console.WriteLine(keyValuePair));





        }
    }
}
