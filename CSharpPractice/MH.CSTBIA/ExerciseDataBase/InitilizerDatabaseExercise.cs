﻿namespace ConsoleApp.MH_DataBaseExercise {
    public class InitilizerDatabaseExercise {
        public void Init() {
            DbConnection dbConnection = new MySqlConnection("driver");
            DbCommand dbCommand = new DbCommand(dbConnection, "clear database");
            dbCommand.Execute();
        }
    }
}