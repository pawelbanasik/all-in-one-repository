﻿using System;
using System.IO;

namespace ConsoleApp._041InnerException {
    public class InitilizerInnerException {

        public void Init() {
            try {
                try {
                    Console.WriteLine("Please enter first number:");
                    int fn = int.Parse(Console.ReadLine());
                    Console.WriteLine("Please enter second number:");
                    int sn = int.Parse(Console.ReadLine());
                    int result = fn / sn;
                    Console.WriteLine(result);
                } catch (Exception e) {
                    string filepath = @"D:\Pobrane\log.txt";
                    if (File.Exists(filepath)) {
                        StreamWriter sw = new StreamWriter(filepath);
                        sw.Write(e.GetType().Name);
                        sw.Close();
                        Console.WriteLine("There is a problem. Please try again.");
                    } else {
                        // we need this exception passed as parameter or nullreferenceexception will happen
                        throw new FileNotFoundException($"{filepath} is not present.", e);
                    }
                }
            } catch (Exception e) {
                Console.WriteLine($"Current exception {e.GetType().Name}");
                Console.WriteLine($"Inner exception {e.InnerException.GetType().Name}");
            }
        }
    }
}