﻿using System;

namespace ConsoleApp.StopWatchExercise {
    public class InitStopWatch {
        public void Init() {
            Console.WriteLine("StopWatch commands start/stop/exit:");
            StopWatch stopWatch = new StopWatch();

            while (true) {
                String input = Console.ReadLine();

                if (input == "start") {
                    stopWatch.Start();
                } else if (input == "stop") {
                    stopWatch.Stop();
                    stopWatch = new StopWatch();
                } else if (input == "exit") {
                    break;
                } else {
                    Console.WriteLine("Invalid input! Try again!");
                }
            }

        }
    }
}