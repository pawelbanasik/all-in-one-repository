﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Castle.Core.Logging;
using Castle.DynamicProxy.Generators.Emitters.SimpleAST;
using ConsoleApp.Section01;
using Machine.Specifications;
using Type = ConsoleApp.Section01.Type;

namespace ConsoleApp.Spec.Section01 {
    class CharacterSpec {
    }

    public class CharacterSpecBase {
        protected static Character Ork;
        protected static Character Elf;
        protected static Character TempCharacter;

        private Establish context = () => {
            Ork = new Character(Type.Ork);
            Elf = new Character(Type.Elf);
            TempCharacter = Elf;
        };
    }

    public class When_Inputing_Name_To_The_Consturctor : CharacterSpecBase {
        Because of = () => Elf = new Character(Type.Elf, "John");
        It Should_Set_The_Right_Name_Using_The_Constructor = () => Elf.Name.ShouldEqual("John");
        It Should_Not_Be_Empty = () => Elf.Name.ShouldNotBeEmpty();
        It Should_Contain_Substring = () => Elf.Name.ShouldContain("ohn");
    }

    public class When_Inputing_Uppercase_Name_To_The_Constructor : CharacterSpecBase {
        Because of = () => Elf = new Character(Type.Elf, "JOHN");
        It Should_Set_The_Right_Name = () => Elf.Name.ShouldEqual("JOHN");
        It Should_Set_The_Right_Name_With_Ignore_Case = () => Elf.Name.ShouldBeEqualIgnoringCase("john");
    }

    public class When_Creating_Ork_Character : CharacterSpecBase {
        Because of = () => Ork = new Character(Type.Ork);
        It Should_Set_The_Default_Health_To_100 = () => Ork.Health.ShouldEqual(100);
        It Should_Set_The_Default_Health_To_Be_A_Positive_Value = () => Ork.Health.ShouldBeGreaterThan(0);
        It Should_Set_The_Default_Speed_To_The_Expected_Value = () => Ork.Speed.ShouldEqual(1.4);
        It Should_Set_The_Default_Speed_To_The_Expected_Value_With_Tolerance = () => Ork.Speed.ShouldBeCloseTo(1.4);
        It Should_Set_TheDefault_Name_To_Null = () => Ork.Name.ShouldBeNull();
    }

    public class When_Doing_Kill_Damage_To_Character : CharacterSpecBase {
        Because of = () => Ork.Damage(500);
        It Should_Kill_Character = () => Ork.IsDead.ShouldEqual(true);
    }

    public class When_Adding_Weapons_To_Character : CharacterSpecBase {
        private Because of = () => {
            Elf.Weaponry.Add("Knife");
            Elf.Weaponry.Add("Pistol");
        };

        private It Should_Weaponry_List_On_Character_Not_Be_Empty = () => Elf.Weaponry.ShouldNotBeEmpty();
        private It Should_Weaponry_List_On_Character_Contains_Knife = () => Elf.Weaponry.ShouldContain("Knife", "Pistol");
    }

    public class When_Checking_Equality_Of_Charactes : CharacterSpecBase {
        private It Should_Show_The_Proper_Equality = () => Elf.ShouldBeLike(TempCharacter);
    }

    public class When_Checking_Type_Of_Character : CharacterSpecBase {
        private It Should_Match_The_Expected_Type_Of_Character = () => Elf.ShouldBeOfExactType<Character>();
    }

    public class When_Checking_Default_Armor_Value_Set_Thru_Constructor : CharacterSpecBase {
        private It Should_Set_Default_Ork_Armor_In_Range = () => Ork.Armor.ShouldBeGreaterThan(90).ShouldBeLessThan(110);
        private It Should_Set_Default_Elf_Armor_In_Range = () => Elf.Armor.ShouldBeGreaterThan(50).ShouldBeLessThan(70);
    }

    public class When_Damage_Is_Set_Above_The_Limit : CharacterSpecBase {
        private static Exception exception;
        private Because of = () => exception = Catch.Exception(() => Elf.Damage(1001));

        private It Should_Throw_Argument_Out_Of_Range_Exception = () => exception.ShouldBeOfExactType<ArgumentOutOfRangeException>();
    }

    public class When_Character_Damaged : CharacterSpecBase {
        private Because of = () => Elf.Damage(100);
        private It Should_Have_Expected_Value_Of_Health = () => Elf.Health.ShouldEqual(45);

    }

    // cannot test multiple parameters in MSpec

    [Subject("Ignoring example")]
    [Ignore("Just ignoring this test")]
    public class Example_How_To_Ignore_Test : CharacterSpecBase {
        private Because of = () => Elf.Damage(100);
        private It Should_Have_Expected_Value_Of_Health = () => Elf.Health.ShouldEqual(45);
    }























}
