﻿namespace Mediator {
    public interface IAirTrafficControl {
        void ReceiveAircraftLocation(Aircraft aircraft);
        void RegisterAircraftUnderGuidance(Aircraft aircraft);
    }
}
