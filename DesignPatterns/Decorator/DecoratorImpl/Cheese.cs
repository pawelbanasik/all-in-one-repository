﻿namespace Decorator {
    public class Cheese : PizzaDecorator {
        public Cheese(Pizza pizza) : base(pizza) {
            Description = "Cheese";
        }

        public override double CalculateCost() {
            return _pizza.CalculateCost() + 1.25;
        }

        public override string GetDescription() {
            return $"Description: {_pizza.GetDescription()}, {Description}";
        }
    }
}
