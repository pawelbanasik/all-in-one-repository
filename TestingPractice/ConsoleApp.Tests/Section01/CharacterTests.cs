﻿using System;
using System.Collections;
using System.Diagnostics;
using ConsoleApp.Section01;
using NUnit.Framework;
using Type = ConsoleApp.Section01.Type;

namespace ConsoleApp.Tests.Section01 {
/*
 runs on the level of assembly - sets up the environment for all the
 tests in the current assembly
 */
    [SetUpFixture]
    class AssemblySetupTeardown {
        [OneTimeSetUp]
        public void AssemblySetup() {
            Trace.WriteLine("Assembly Setup!");
        }

        [OneTimeTearDown]
        public void AssemblyTeardown() {
            Trace.WriteLine("Assembly Teardown!");
        }
    }

/*
runs on the level of namespace - sets up the environment for
the whole namespace
*/
    [SetUpFixture]
    class NamespaceSetupTeardown {
        [OneTimeSetUp]
        public void NamespaceSetup() {
            Trace.WriteLine("Namespace setup!");
        }

        [OneTimeTearDown]
        public void NamespaceTeardown() {
            Trace.WriteLine("Namespace teardown!");
        }

    }


    [TestFixture]
    class CharacterTests {

        private Character elf;
        private Character ork;

        [SetUp]
        public void Setup() {
            elf = new Character(Type.Elf);
            ork = new Character(Type.Ork);

        }

        [TearDown]
        public void Teardown() {
            elf.Dispose();
            ork.Dispose();
        }

        [Test]
        public void Constructor_InputingName_NameSetAsExpected() {
            Character character = new Character(Type.Elf, "John");
            string expected = "John";

            Assert.That(character.Name, Is.EqualTo(expected));
            Assert.That(character.Name, Is.Not.Empty);
            Assert.That(character.Name, Contains.Substring("ohn"));
        }

        [Test]
        public void Constructor_InputingNameUpperCase_NameSetAsExpected() {
            Character character = new Character(Type.Elf, "JOHN");
            string expectedUpperCase = "JOHN";
            string expectedLowerCase = "john";

            Assert.That(character.Name, Is.EqualTo(expectedUpperCase));
            Assert.That(character.Name, Is.EqualTo(expectedLowerCase).IgnoreCase);


        }
        [Test]
        public void SetDefaultHealthProperty_Is100_Value100AsExpected() {

            int expectedHealth = 100;

            Assert.That(ork.Health, Is.EqualTo(expectedHealth));
            Assert.That(ork.Health, Is.Positive);
        }

        [Test]
        public void ConstructorSetSpeedBasedOnRace_Is1p4_ValueExpected1p4() {
            double expectedSpeed = 1.4;
            Assert.That(ork.Speed, Is.EqualTo(expectedSpeed));

        }

        [Test]
        public void Constructor_SpeedSetBasedOnRaceWithTolerance_ValueWithinTolerance() {
            double expectedSpeed = 0.3 + 1.1;

            Assert.That(ork.Speed, Is.EqualTo(expectedSpeed).Within(0.01));
            Assert.That(ork.Speed, Is.EqualTo(expectedSpeed).Within(1).Percent);
        }

        [Test]
        public void Constructor_DefaultNameIsNull_NullAsExpected() {
            string expectedName = null;
            Assert.That(elf.Name, Is.Null);
        }

        [Test]
        public void Damage_DoKillDamage_ExpectCharacterDead() {
            // given/arrange
            Character character = new Character(Type.Elf); // this can be in setup

            // when/act
            character.Damage(500);

            // then/assert
            Assert.That(character.IsDead, Is.True);
        }


        [Test]
        public void Constructor_AddingWeaponsToEmbeddedList_ElementsPresentInList() {
            Character character = new Character(Type.Elf);
            character.Weaponry.Add("Knife");
            character.Weaponry.Add("Pistol");

            Assert.That(character.Weaponry, Is.All.Not.Empty);
            Assert.That(character.Weaponry, Contains.Item("Knife"));
            Assert.That(character.Weaponry, Contains.Item("Pistol"));
            Assert.That(character.Weaponry, Has.Exactly(2).Length);
            Assert.That(character.Weaponry, Is.Unique);
            Assert.That(character.Weaponry, Is.Ordered);

            Character character2 = new Character(Type.Elf);
            character2.Weaponry.Add("Knife");
            character2.Weaponry.Add("Pistol");

            Assert.That(character.Weaponry, Is.EquivalentTo(character2.Weaponry));

        }

        [Test]
        public void OverallEqualityCheck() {
            Character ch1 = new Character(Type.Elf);
            Character ch2 = ch1;

            Assert.That(ch1, Is.SameAs(ch2));

        }

        [Test]
        public void OverallTypeCheck() {
            object obj = elf;
            Assert.That(obj, Is.TypeOf<Character>());

        }

        [Test]
        public void Constructor_ProperSettingOfDefaultArmorValue_DefaultValueInRange() {
            Assert.That(elf.Armor, Is.GreaterThan(50).And.LessThan(70));
            Assert.That(ork.Armor, Is.GreaterThan(90).And.LessThan(110));
            Assert.That(ork.Armor, Is.InRange(90, 110));
        }

        [Test]
        public void Damage_SetDamageAboveLimit_ThrowException() {
            Assert.Throws<ArgumentOutOfRangeException>(() => elf.Damage(1001));
            Assert.That(() => elf.Damage(1001), Throws.TypeOf<ArgumentOutOfRangeException>());


        }

        // Parametrized test - 1st way
        [TestCase(100, 45)]
        [TestCase(80, 65)]
        public void Damage_DamageTheCharacter_CheckIfHealthHasExpectedValue(int damage, int expectedHealth) {
            elf.Damage(damage);
            Assert.That(elf.Health, Is.EqualTo(expectedHealth));
        }

        // Parametrized test - 2nd way
        [TestCaseSource(typeof(DamageSource))]
        public void Damage_DamageTheCharacter_CheckIfHealthHasExpectedValue2(int damage, int expectedHealth) {
            elf.Damage(damage);
            Assert.That(elf.Health, Is.EqualTo(expectedHealth));
        }

        // This class is needed for parametrized test 2nd way
        public class DamageSource: IEnumerable {
            public IEnumerator GetEnumerator() {
                yield return new [] { 100, 45 };
                yield return new [] { 80, 65 };
                
            }
        }

        // Category and ignore (we can put category on class also)
        [Test]
        [Category("Slow")]
        [Ignore("This is slow")]
        public void ExampleHowToIgnoreTests() {
            Character character = new Character(Type.Elf, "John");
            string expected = "John";

            Assert.That(character.Name, Is.EqualTo(expected));
            Assert.That(character.Name, Is.Not.Empty);
            Assert.That(character.Name, Contains.Substring("ohn"));
        }


    }
}