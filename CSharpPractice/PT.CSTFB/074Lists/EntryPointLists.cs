﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApp._074Lists {
    public class Customer {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Salary { get; set; }

        public Customer(int id, string name, int salary) {
            Id = id;
            Name = name;
            Salary = salary;
        }

        public override string ToString() {
            StringBuilder sb = new StringBuilder();
            sb.Append($"Id: {Id}, ");
            sb.Append($"Name: {Name}, ");
            sb.Append($"Salary: {Salary}");
            return sb.ToString();
        }

    }

    public class SavingsCustomer : Customer{
        public SavingsCustomer(int id, string name, int salary) : base(id, name, salary) {
        }
    }

    class Program {
        static void MainLists(string[] args) {
            Customer customer1 = new Customer(101, "Mark", 5000);
            Customer customer2 = new Customer(110, "Pam", 6500);
            Customer customer3 = new Customer(119, "John", 3500);

            Customer[] customersArray = new Customer[2];
            customersArray[0] = customer1;
            customersArray[1] = customer2;
            // error doesn't grow in size
            // customers[2] = customer3; 

            List<Customer> customers = new List<Customer>(2);
            customers.Add(customer1);
            customers.Add(customer2);
            // auto grow in size
            customers.Add(customer3);

            //can add subclass
            customers.Add(new SavingsCustomer(121, "Grzegorz", 3000));

            Customer customer = customers[0];
            Console.WriteLine(customer);

            customers.ForEach(c => Console.WriteLine(c));

            foreach (Customer ctr in customers) {
                Console.WriteLine(ctr);
            }

            int indexOf = customers.IndexOf(customer3);
            Console.WriteLine(indexOf);

            // Contains()
            if (customers.Contains(customer3)) {
                Console.WriteLine("Customer3 object exists in the list.");
            } else {
                Console.WriteLine("Customer3 doesn't exists in the list.");
            }

            // Exists()
            if (customers.Exists(c => c.Name.StartsWith("P"))) {
                Console.WriteLine("Customer starting with p exists in the list");
            } else {
                Console.WriteLine("Customer starting with p doesn't exists in the list.");
            }

            // Find()
            Customer find = customers.Find(c => c.Salary > 5000);
            Console.WriteLine(find);

            // FindLast()
            Customer findLast = customers.FindLast(c => c.Salary > 5000);
            Console.WriteLine(findLast);

            // FindAll()
            List<Customer> all = customers.FindAll(c => c.Salary > 4000);
            all.ForEach(c => Console.WriteLine(c));

            // FindIndex()
            int index = customers.FindIndex(c => c.Salary > 5000);
            Console.WriteLine(index);

            Console.WriteLine("------------------------------------");

            // Convert array to list
            List<Customer> list = customersArray.ToList();
            list.ForEach(c => Console.WriteLine(c));

            // Convert list to array
            Customer[] array = list.ToArray();
            Array.ForEach(array, c => Console.WriteLine(c));

            Console.WriteLine("--------");

            // Convert list to dictionary
            Dictionary<int, Customer> dictionary = list.ToDictionary(c => c.Id);
            dictionary.ToList().ForEach(kvp => Console.WriteLine(kvp));

        }
    }
}
