﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Observer {
    class Program {
        static void Main(string[] args) {
            // 1. Motivating example
            // Monitor a stock ticker, when particular events occur, react
            //foreach (Stock s in SampleData.getNext()) {
            // Reactive Filters
            //if (s.Symbol == "GOOG")
            //    Console.WriteLine("Google's new price is: {0}", s.Price);

            //if (s.Symbol == "MSFT" && s.Price > 10.00m)
            //    Console.WriteLine("Microsoft has reached the target price: {0}", s.Price);

            //if (s.Symbol == "XOM")
            //    Console.WriteLine("Exxon mobile's price is {0}", s.Price);

            // 2. Traditional
            // Monitor a stock ticker, when particular events occur, react
            //StockTicker subj = new StockTicker();

            // Create New observers to listen to the stock ticker
            //GoogleObserver gobs = new GoogleObserver(subj);
            //MicrosoftObserver mobs = new MicrosoftObserver(subj);

            // Load the Sample Stock Data
            //foreach (var s in SampleData.getNext())
            //    subj.Stock = s;

            // 3. Event and delegate
            // Monitor a stock ticker, when particular events occur, react
            //StockTicker st = new StockTicker();

            // Create New observers to listen to the stock ticker
            //GoogleMonitor gf = new GoogleMonitor(st);
            //MicrosoftMonitor mf = new MicrosoftMonitor(st);

            // Load the Sample Stock Data
            //foreach (var s in SampleData.getNext())
            //    st.Stock = s;

            // 4. IObserver
            // Monitor a stock ticker, when particular events occur, react
            //StockTicker st = new StockTicker();

            //GoogleMonitor gf = new GoogleMonitor();
            //MicrosoftMonitor mf = new MicrosoftMonitor();

            // Load the Sample Stock Data
            //using (st.Subscribe(gf))
            //using (st.Subscribe(mf)) {
            //foreach (var s in SampleData.getNext())
            //    st.Stock = s;
        }
    }
}
