﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp._076ListsContinued {
    public class Customer {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Salary { get; set; }
        public string Type { get; set; }

        public Customer(int id, string name, int salary, string type) {
            Id = id;
            Name = name;
            Salary = salary;
            Type = type;
        }

        public override string ToString() {
            StringBuilder sb = new StringBuilder();
            sb.Append($"Id: {Id}, ");
            sb.Append($"Name: {Name}, ");
            sb.Append($"Salary: {Salary}, ");
            sb.Append($"Type: {Type}.");
            return sb.ToString();
        }
    }

    class Program {
        static void MainAnother(string[] args) {
            Customer customer1 = new Customer(101, "Mark", 4000, "RetailCustomer");
            Customer customer2 = new Customer(102, "Pam", 7000, "RetailCustomer");
            Customer customer3 = new Customer(103, "Rob", 5500, "RetailCustomer");
            Customer customer4 = new Customer(104, "John", 6500, "CorporateCustomer");
            Customer customer5 = new Customer(105, "Sam", 3500, "CorporateCustomer");

            List<Customer> listCustomers = new List<Customer>();
            listCustomers.Add(customer1);
            listCustomers.Add(customer2);
            listCustomers.Add(customer3);

            List<Customer> listCorporateCustomers = new List<Customer>();
            listCorporateCustomers.Add(customer4);
            listCorporateCustomers.Add(customer5);

            listCustomers.AddRange(listCorporateCustomers);
            listCustomers.ForEach(c => Console.WriteLine(c));

            Console.WriteLine("------");

            // GetRange()
            List<Customer> range = listCustomers.GetRange(3, 2);
            range.ForEach(c => Console.WriteLine(c));

            Console.WriteLine("-------");

            // InsertRange()
            listCustomers.InsertRange(0, listCorporateCustomers);
            listCustomers.ForEach(c => Console.WriteLine(c));

            Console.WriteLine("------");
            // RemoveRange()
            listCustomers.RemoveRange(5, 2);
            listCustomers.ForEach(c => Console.WriteLine(c));
        }
    }
}
