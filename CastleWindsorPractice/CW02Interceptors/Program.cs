﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Castle.Core;
using Castle.DynamicProxy;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Castle.Windsor.Installer;

namespace CW02Interceptors {

    public interface ILogger {
        void Info(string message);
        void Error(string message);
    }

    public class ConsoleLogger : ILogger {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public void Info(string message) {
            log.Info(message);
        }

        public void Error(string message) {
            log.Error(message);
        }
    }

    public interface ISomething {
        int Augment(int input);
        void DoSomething(string input);
        int Property { get; set; }
    }

    [Interceptor(typeof(DumpInterceptor))]
    public class Something : ISomething {
        public int Augment(int input) {
            return input + 1;
        }

        public void DoSomething(string input) {
            Console.WriteLine($"I'm doing something: {input}");
        }

        public int Property { get; set; }
    }

    public class DumpInterceptor : IInterceptor {
        private ILogger logger;

        public DumpInterceptor(ILogger logger) {
            this.logger = logger;
        }

        public void Intercept(IInvocation invocation) {
            logger.Info($"Dump interceptor called on {invocation.Method.Name}");
            invocation.Proceed();
            if (invocation.Method.ReturnType == typeof(int)) {
                invocation.ReturnValue = (int)invocation.ReturnValue + 1;
            }
            logger.Info($"DumpInterceptor return value is {invocation.ReturnValue ?? "NULL"}");
        }
    }

    public class MyInstaller : IWindsorInstaller {
        public void Install(IWindsorContainer container, IConfigurationStore store) {
            container.Register(
                Component.For<IInterceptor>().ImplementedBy<DumpInterceptor>(),
                Component.For<ISomething>().ImplementedBy<Something>(),
                Component.For<ILogger>().ImplementedBy<ConsoleLogger>()
            );
        }
    }

    class Program {
        static void Main(string[] args) {

            WindsorContainer container = new WindsorContainer();
            container.Install(FromAssembly.This());
            ISomething something = container.Resolve<ISomething>();
            ILogger logger = container.Resolve<ILogger>();
            something.DoSomething("test");
            // interceptors can modify return values!
            logger.Error($"Augment 10 returns {something.Augment(10)}");
            container.Dispose();
        }
    }
}
