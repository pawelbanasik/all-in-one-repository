﻿using System;

namespace Command {
    public class UpdateQuantityCommand : ICommand, ICommandFactory {
        public int NewQuantity { get; set; }
        public string Name { get { return "UpdateQuantity"; } }
        public string Description { get { return "UpdateQuantity number"; } }

        public ICommand MakeCommand(string[] arguments) {
            return new UpdateQuantityCommand { NewQuantity = int.Parse(arguments[1]) };
        }

        public void Execute() {
            const int oldQuantity = 5;
            Console.WriteLine("Database updated.");
            Console.WriteLine($"LOG: Updated order quantity from {oldQuantity} to {NewQuantity}", oldQuantity, NewQuantity);
        }
    }
}