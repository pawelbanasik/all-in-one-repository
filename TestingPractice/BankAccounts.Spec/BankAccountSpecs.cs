﻿using Machine.Specifications;
using NSubstitute;
using System;


namespace BankAccounts.Spec {

    // this is inheritance (there is also nested classes with another Establish)
    public abstract class TransferSpecsBase {
        // needs static to be accesible from lambda expression
        protected static Account FromAccount;
        protected static Account ToAccount;
        protected static IEmailGateway EmailGateway;
        protected static Transfer TransferManager;

        private Establish context = () => {
            FromAccount = new Account(1m);
            ToAccount = new Account(1m);
            EmailGateway = Substitute.For<IEmailGateway>();
            TransferManager = new Transfer(EmailGateway);
        };

        protected static void Transfer(decimal amountToTransfer) {
            TransferManager.TransferFunds(FromAccount, ToAccount, amountToTransfer);
        }
    }

    [Subject("Transferring Money")]
    public class When_transferring_between_two_accounts : TransferSpecsBase {
        Because of = () =>
            Transfer(.5m);

        It Should_debit_the_from_account_by_the_amount_transferred = () =>
            FromAccount.Balance.ShouldEqual(.5m);

        It Should_credit_the_to_account_by_the_amount_transferred = () =>
            ToAccount.Balance.ShouldEqual(1.5m);

        It Should_send_an_email_with_the_subject_funds_transferred_to_the_account_holder = () =>
        EmailGateway.Received(1).SendEmail("Funds transferred", "bdd@bank.com", FromAccount.EmailAddress, $"Successfully transfered funds in the amoumt of: 0,5");
    }

    [Subject("Transferring Money")]
    public class When_transferring_an_amount_greater_than_the_balance_of_the_from_account : TransferSpecsBase {
        // we need this field to hold the exception
        static Exception TransferException;

        Because of = () =>
            TransferException = Catch.Exception(() => Transfer(1.1m));

        It Should_not_allow_the_transfer = () =>
            TransferException.ShouldNotBeNull();

        It Should_throw_an_invalid_operation_exception = () =>
            TransferException.ShouldBeOfExactType<InvalidOperationException>();

        It Should_include_the_requested_amount_in_the_exception_message = () =>
            TransferException.Message.ShouldContain("1,1");

        It Should_include_the_current_balance_in_the_exception_message = () =>
            TransferException.Message.ShouldContain("1");
    }

}