﻿using System.Collections.Generic;

namespace ConsoleApp.WorkflowExercise {
    public interface IWorkflow {
        List<IActivity> List { get; }
        void RegisterActivity(IActivity activity);
    }
}