﻿using System;

namespace BuilderWithState {
    class Program {
        static void Main(string[] args) {
            PersonBuilder builder = new PersonBuilder();
            builder.SetFirstName("Max");
            builder.SetLastName("Planck");
            Person person = builder.Build();
            Console.WriteLine(person);
        }
    }
}
