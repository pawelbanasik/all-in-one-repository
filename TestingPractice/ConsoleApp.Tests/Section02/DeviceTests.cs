﻿using System;
using System.Threading.Tasks;
using ConsoleApp.Section02;
using NSubstitute;
using NUnit.Framework;

namespace ConsoleApp.Tests.Section02 {
    [TestFixture]
    public class DeviceTests {

        [Test]
        public void Connect_FailedThrice_ThreeTries() {
            IProtocol protocolMock = Substitute.For<IProtocol>();
            string anyString = Arg.Any<string>();
            protocolMock.Connect(anyString).Returns(false);

            // expression examples
            // protocolMock.Connect(Arg.Is("COM1")).Returns(true);
            // protocolMock.Connect(Arg.Is<string>(x => x.StartsWith("COM"))).Returns(true);

            Device device = new Device(protocolMock);

            bool actual = device.Connect(anyString);

            bool expected = false;

            Assert.That(actual, Is.EqualTo(expected));

            protocolMock.Received(3).Connect(Arg.Any<string>());
        }

        // Events in unit testing
        [Test]
        public void Find_FoundOnCOM1_ReturnsCOM1() {
            IProtocol protocolMock = Substitute.For<IProtocol>();
            Device device = new Device(protocolMock);
            Task<string> task = device.Find();

            string portName = "COM1";

            protocolMock.SearchingFinished += Raise.Event<EventHandler<DeviceSearchingEventArgs>>
                (protocolMock, new DeviceSearchingEventArgs(portName));

            Assert.That(task.Result, Is.EqualTo(portName));
        }

    }
}