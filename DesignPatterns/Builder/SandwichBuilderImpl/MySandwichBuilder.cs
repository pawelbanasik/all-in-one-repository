﻿using System.Collections.Generic;

namespace Builder {
    public class MySandwichBuilder : SandwichBuilder {
        public override void PrepareBread() {
            sandwich.BreadType = BreadType.White;
        }

        public override void ApplyMeatAndCheese() {
            sandwich.CheeseType = CheeseType.Cheddar;
            sandwich.MeatType = MeatType.Bacon;
        }

        public override void AddCondiments() {
            sandwich.HasMayo = false;
            sandwich.IsToasted = true;
            sandwich.HasMustard = true;
        }

        public override void ApplyVegetables() {
            sandwich.Vegetables = new List<string> { "Tomato", "Onion" };
        }
    }
}
