﻿namespace ConsoleApp.DependancyInjectionWithList {
    public interface INotificationChannel {
        void Send(Message message);
    }
}