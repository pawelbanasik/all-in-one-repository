﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp.DependencyInjection {
    class Initializer {
        public static void InitilizeApp() {
            DbMigrator dbMigrator = new DbMigrator(new ConsoleLogger());
            dbMigrator.Migrate();

            DbMigrator dbMigrator2 = new DbMigrator(new FileLogger(@"D:\Pobrane\log.txt"));
            dbMigrator2.Migrate();

        }

    }
}
