﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleApp.Section02;
using Machine.Specifications;
using NSubstitute;

namespace ConsoleApp.Spec.Section02 {
    class DeviceSpec {
    }

    class DeviceSpecBase {
        protected static IProtocol protocol;
        protected static Device device;

        private Establish context = () => {
            protocol = Substitute.For<IProtocol>();
            device = new Device(protocol);
        };
    }


    class When_Connect_Failed_Three_Times : DeviceSpecBase {
        private Because of = () => {
            protocol.Connect(Arg.Any<string>()).ReturnsForAnyArgs(false);
        };

        private It Should_Try_To_Establish_Connection_Three_Times = () => {
            device.Connect("test").ShouldEqual(false);
            protocol.Received(3).Connect(Arg.Any<string>());
        };
    }

    // test for events here
}
