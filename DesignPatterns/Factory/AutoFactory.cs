using System;
using System.Collections.Generic;
using System.Reflection;

namespace Factory {
    public class AutoFactory {
        public Dictionary<string, Type> Autos { get; private set; }

        public AutoFactory() {
            LoadTypesICanReturn();
        }

        public IAuto CreateInstance(string carName) {
            Type t = GetTypeToCreate(carName);
            if (t == null) {
                return new NullAuto();
            }
            return Activator.CreateInstance(t) as IAuto;
        }

        private Type GetTypeToCreate(string carName) {
            foreach (var auto in Autos) {
                if (auto.Key.Contains(carName)) {
                    return Autos[auto.Key];
                }
            }
            return null;
        }

        private void LoadTypesICanReturn() {
            Autos = new Dictionary<string, Type>();
            Type[] typesInThisAssembly = Assembly.GetExecutingAssembly().GetTypes();
            foreach (Type type in typesInThisAssembly) {
                if (type.GetInterface(typeof(IAuto).ToString()) != null) {
                    Autos.Add(type.Name.ToLower(), type);
                }
            }
        }
    }
}