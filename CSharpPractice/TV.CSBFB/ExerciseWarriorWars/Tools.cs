﻿using System;

namespace ConsoleApp.TV_WarriorWarsExercise {
    public static class Tools {
        public static void ColorfullWriteLine(string text, ConsoleColor color) {
            Console.ForegroundColor = color;
            Console.WriteLine(text);
            Console.ResetColor();
        }
    }
}