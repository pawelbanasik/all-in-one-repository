﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleApp.Section02;

// stub
namespace ConsoleApp.Tests.Section02 {
    public class DbGatewayStub : IDbGateway {

        private WorkingStatistics ws;

        public WorkingStatistics GetWorkingStatistics(int id) {
            return ws;
        }

        public void SetWorkingStatistics(WorkingStatistics ws) {
            this.ws = ws;
        }

        public bool Connected { get; }
        
    }
}
