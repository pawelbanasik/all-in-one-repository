﻿namespace Mediator {
    public class Boeing737200 : Aircraft {
        public Boeing737200(IAirTrafficControl airTrafficControl, string callSign) : base(airTrafficControl, callSign) {
        }

        public override int Ceiling => 50000;
    }
}
