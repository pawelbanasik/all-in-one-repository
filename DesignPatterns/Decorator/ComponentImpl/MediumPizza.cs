﻿namespace Decorator {
    public class MediumPizza : Pizza {
        public MediumPizza() {
            Description = "Medium Pizza";
        }

        public override double CalculateCost() {
            return 4.50;
        }

        public override string GetDescription() {
            return Description;
        }
    }
}
